#include "credits.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace credits
	{
		static Color backButton = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else backButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawText("PROGRAMMER", pos::creditsPosX, pos::creditsPosY, sizes::textSize3, WHITE);
			DrawText("Micaela Luz Benitez", pos::creditsPosX, pos::creditsPosY2, sizes::textSize4, WHITE);

			DrawText("TEXTURES", pos::creditsPosX, pos::creditsPosY3, sizes::textSize3, WHITE);
			DrawText("Micaela Luz Benitez", pos::creditsPosX, pos::creditsPosY4, sizes::textSize4, WHITE);
			DrawText("sheep from opengameart.org", pos::creditsPosX, pos::creditsPosY5, sizes::textSize4, WHITE);
			DrawText("sheep & Radomir Dopieralski", pos::creditsPosX, pos::creditsPosY6, sizes::textSize4, WHITE);
			DrawText("from opengameart.org", pos::creditsPosX, pos::creditsPosY7, sizes::textSize4, WHITE);
			DrawText("galangpiliang from opengameart.org", pos::creditsPosX, pos::creditsPosY8, sizes::textSize4, WHITE);
			DrawText("Alucard from opengameart.org", pos::creditsPosX, pos::creditsPosY9, sizes::textSize4, WHITE);
			DrawText("BlueRatDesigns from opengameart.org", pos::creditsPosX, pos::creditsPosY10, sizes::textSize4, WHITE);
			DrawText("Jattenalle from opengameart.org", pos::creditsPosX, pos::creditsPosY11, sizes::textSize4, WHITE);
			DrawText("stickpng.com", pos::creditsPosX, pos::creditsPosY12, sizes::textSize4, WHITE);
			DrawText("pngtree.com", pos::creditsPosX, pos::creditsPosY13, sizes::textSize4, WHITE);

			DrawText("MUSIC", pos::creditsPosX2, pos::creditsPosY5, sizes::textSize3, WHITE);
			DrawText("freesound.org", pos::creditsPosX3, pos::creditsPosY6, sizes::textSize4, WHITE);

			DrawText("SOUNDS", pos::creditsPosX4, pos::creditsPosY8, sizes::textSize3, WHITE);
			DrawText("freesound.org", pos::creditsPosX5, pos::creditsPosY9, sizes::textSize4, WHITE);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{

		}
	}
}