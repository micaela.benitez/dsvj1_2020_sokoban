#ifndef GAME_H
#define GAME_H

#include "scenes/menu/menu.h"
#include "scenes/levels/levels.h"
#include "scenes/change_character/change_character.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/instructions/instructions.h"
#include "scenes/settings/settings.h"
#include "scenes/settings/game_audio/game_audio.h"
#include "scenes/settings/resolutions/resolutions.h"
#include "scenes/credits/credits.h"

namespace sokoban
{
	namespace game
	{
		enum class Scene { MENU, LEVELS, CHANGECHARACTER, GAMEPLAY, INSTRUCTIONS, SETTINGS, AUDIO, RESOLUTIONS, CREDITS };

		extern Scene currentScene;
		extern Scene previousScene;

		extern bool exitButton;

		static void init();
		static void update();
		static void draw();
		static void deinit();

		void run();
	}
}

#endif