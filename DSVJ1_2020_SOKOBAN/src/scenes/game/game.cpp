#include "game.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace game
	{
		Scene currentScene;
		Scene previousScene;

		bool exitButton;

		static void init()
		{
			InitWindow(screenWidth, screenHeight, "SOKOBAN");
			InitAudioDevice();
			SetTargetFPS(60);

			currentScene = Scene::MENU;

			menu::init();
			levels::init();
			change_character::init();
			gameplay::init();
			instructions::init();
			settings::init();
			game_audio::init();
			resolutions::init();
			credits::init();
		}

		static void update()
		{
			sizes::updateSizes();
			texture::updateTextures();
			pos::updateVariables();

			switch (currentScene)
			{
			case Scene::MENU:
				menu::update();
				break;
			case Scene::LEVELS:
				levels::update();
				break;
			case Scene::CHANGECHARACTER:
				change_character::update();
				break;
			case Scene::GAMEPLAY:
				gameplay::update();
				break;
			case Scene::INSTRUCTIONS:
				instructions::update();
				break;
			case Scene::SETTINGS:
				settings::update();
				break;
			case Scene::AUDIO:
				game_audio::update();
				break;
			case Scene::RESOLUTIONS:
				resolutions::update();
				break;
			case Scene::CREDITS:
				credits::update();
				break;
			}
		}

		static void draw()
		{
			BeginDrawing();
			ClearBackground(DARKGREEN);

			switch (currentScene)
			{
			case Scene::MENU:
				menu::draw();
				break;
			case Scene::LEVELS:
				levels::draw();
				break;
			case Scene::CHANGECHARACTER:
				change_character::draw();
				break;
			case Scene::GAMEPLAY:
				gameplay::draw();
				break;
			case Scene::INSTRUCTIONS:
				instructions::draw();
				break;
			case Scene::SETTINGS:
				settings::draw();
				break;
			case Scene::AUDIO:
				game_audio::draw();
				break;
			case Scene::RESOLUTIONS:
				resolutions::draw();
				break;
			case Scene::CREDITS:
				credits::draw();
				break;
			}

			EndDrawing();
		}

		static void deinit()
		{
			CloseWindow();

			menu::deinit();
			levels::deinit();
			change_character::deinit();
			gameplay::deinit();
			instructions::deinit();
			settings::deinit();
			game_audio::deinit();
			resolutions::deinit();
			credits::deinit();
		}

		void run()
		{
			init();

			while (!WindowShouldClose() && !exitButton)
			{
				update();
				draw();
			}

			deinit();
		}
	}
}