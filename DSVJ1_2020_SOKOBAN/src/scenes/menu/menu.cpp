#include "menu.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace menu
	{
		static Color playButton = WHITE;
		static Color instructionsButton = WHITE;
		static Color settingsButton = WHITE;
		static Color creditsButton = WHITE;
		static Color exitButton = WHITE;

		void init()
		{
			texture::background = LoadTexture("res/assets/textures/background.png");
			texture::background.width = screenWidth;
			texture::background.height = screenHeight;

			texture::title = LoadTexture("res/raw/textures/title.png");
			texture::title.width = sizes::titleWidth;
			texture::title.height = sizes::titleHeight;

			texture::playButton = LoadTexture("res/raw/textures/playbutton.png");
			texture::playButton.width = sizes::buttonsWidth;
			texture::playButton.height = sizes::buttonsHeight;

			texture::instructionsButton = LoadTexture("res/raw/textures/instructionsbutton.png");
			texture::instructionsButton.width = sizes::buttonsWidth;
			texture::instructionsButton.height = sizes::buttonsHeight;

			texture::settingsButton = LoadTexture("res/raw/textures/settingsbutton.png");
			texture::settingsButton.width = sizes::buttonsWidth;
			texture::settingsButton.height = sizes::buttonsHeight;

			texture::creditsButton = LoadTexture("res/raw/textures/creditsbutton.png");
			texture::creditsButton.width = sizes::buttonsWidth;
			texture::creditsButton.height = sizes::buttonsHeight;

			texture::exitButton = LoadTexture("res/raw/textures/exitbutton.png");
			texture::exitButton.width = sizes::buttonsWidth;
			texture::exitButton.height = sizes::buttonsHeight;

			// Audio
			audio::loadSounds();
			audio::loadMusic();
			audio::setSoundsVolume();
			audio::setMusicVolume();
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY, (float)texture::playButton.width, (float)texture::playButton.height }))
			{
				playButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::LEVELS;
					initialize();
				}
			}
			else playButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY2, (float)texture::instructionsButton.width, (float)texture::instructionsButton.height }))
			{
				instructionsButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::INSTRUCTIONS;
				}
			}
			else instructionsButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY3, (float)texture::settingsButton.width, (float)texture::settingsButton.height }))
			{
				settingsButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
					game::previousScene = game::Scene::MENU;
				}
			}
			else settingsButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY4, (float)texture::creditsButton.width, (float)texture::creditsButton.height }))
			{
				creditsButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CREDITS;
				}
			}
			else creditsButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::menuPosX, pos::menuPosY5, (float)texture::exitButton.width, (float)texture::exitButton.height }))
			{
				exitButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::exitButton = true;
				}
			}
			else exitButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawTexture(texture::title, pos::titlePosX, pos::titlePosY, WHITE);

			DrawTexture(texture::playButton, pos::menuPosX, pos::menuPosY, playButton);
			DrawTexture(texture::instructionsButton, pos::menuPosX, pos::menuPosY2, instructionsButton);
			DrawTexture(texture::settingsButton, pos::menuPosX, pos::menuPosY3, settingsButton);
			DrawTexture(texture::creditsButton, pos::menuPosX, pos::menuPosY4, creditsButton);
			DrawTexture(texture::exitButton, pos::menuPosX, pos::menuPosY5, exitButton);

			DrawText("V 2.0", pos::versionPosX, pos::versionPosY, sizes::textSize4, GRAY);
		}

		void deinit()
		{
			UnloadTexture(texture::title);
			UnloadTexture(texture::playButton);
			UnloadTexture(texture::instructionsButton);
			UnloadTexture(texture::settingsButton);
			UnloadTexture(texture::creditsButton);
			UnloadTexture(texture::exitButton);
		}
	}
}