#ifndef CHANGE_CHARACTER_H
#define CHANGE_CHARACTER_H

#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace change_character
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif