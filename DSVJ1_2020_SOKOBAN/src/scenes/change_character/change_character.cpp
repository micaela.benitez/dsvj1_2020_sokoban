#include "change_character.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace change_character
	{
		static Color girl = WHITE;
		static Color boy = DARKGRAY;
		static Color backButton = WHITE;

		void init()
		{
			texture::girl2 = LoadTexture("res/assets/textures/girl2.png");
			texture::girl2.width = sizes::entitiesWidth;
			texture::girl2.height = sizes::entitiesHeight;

			texture::boy2 = LoadTexture("res/assets/textures/boy2.png");
			texture::boy2.width = sizes::entitiesWidth;
			texture::boy2.height = sizes::entitiesHeight;

			texture::titleCharacter = LoadTexture("res/raw/textures/titlecharacter.png");
			texture::titleCharacter.width = sizes::titleEntitiesWidth;
			texture::titleCharacter.height = sizes::titleEntitiesHeight;

			texture::backButton = LoadTexture("res/raw/textures/backbutton.png");
			texture::backButton.width = sizes::buttonsWidth;
			texture::backButton.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::girlCharacterPosX, pos::charactersPosY, (float)texture::girl2.width, (float)texture::girl2.height }) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)))
			{
				sounds.button = true;
				girl = WHITE;
				boy = DARKGRAY;
				character.texture = texture::girl;
			}

			if (CheckCollisionPointRec(mousePoint, { pos::boyCharacterPosX, pos::charactersPosY, (float)texture::boy2.width, (float)texture::boy2.height }) && (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)))
			{
				sounds.button = true;
				girl = DARKGRAY;
				boy = WHITE;
				character.texture = texture::boy;
			}

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::LEVELS;
				}
			}
			else backButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawTexture(texture::titleCharacter, (screenWidth / 2) - (texture::titleCharacter.width / 2), pos::changeCharacterPosY, WHITE);

			DrawTexture(texture::girl2, pos::girlCharacterPosX, pos::charactersPosY, girl);
			DrawTexture(texture::boy2, pos::boyCharacterPosX, pos::charactersPosY, boy);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{
			UnloadTexture(texture::girl2);
			UnloadTexture(texture::boy2);
			UnloadTexture(texture::titleCharacter);
			UnloadTexture(texture::backButton);
		}
	}
}