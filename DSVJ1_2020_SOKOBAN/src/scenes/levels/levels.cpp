#include "levels.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace levels
	{
		static Color changeCharacterButton = WHITE;
		static Color backButton = WHITE;

		void init()
		{
			if (!changedResolution)
			{
				texture::level = LoadTexture("res/raw/textures/levelssquare.png");
				texture::level.width = sizes::levelsSize;
				texture::level.height = sizes::levelsSize;

				texture::star = LoadTexture("res/assets/textures/star.png");
				texture::star.width = sizes::starsSize;
				texture::star.height = sizes::starsSize;

				texture::changeCharacterButton = LoadTexture("res/raw/textures/changecharacter.png");
				texture::changeCharacterButton.width = sizes::buttonsWidth;
				texture::changeCharacterButton.height = sizes::buttonsHeight;
			}

			static const int spaceBetweenLevels = screenWidth / 40;
			static const int spaceBetweenLines = screenWidth / 15;
			static const int levelsPerLine = 5;
			static const int spaceForStars = screenWidth / 40;

			for (int i = 0; i < quantityLevels; i++)
			{
				if (i < levelsPerLine)
				{
					level[i].position.x = i * (sizes::levelsSize + spaceBetweenLevels) + spaceBetweenLevels / 2;
					level[i].position.y = (screenHeight / 2) - sizes::levelsSize - spaceBetweenLines - spaceForStars;
				}
				else
				{
					level[i].position.x = level[i - levelsPerLine].position.x;
					level[i].position.y = (screenHeight / 2) + spaceBetweenLines - spaceForStars;
				}
			}
		}

		void update()
		{
			int quantityLevelsAvailable = 0;
			
			for (int i = 0; i < quantityLevels; i++) level[i].color = DARKGRAY;

			for (int i = 0; i < quantityLevels; i++)
			{			
				if (level[i].completed)
				{
					quantityLevelsAvailable++;
					level[i].color = WHITE;
					level[i + 1].color = WHITE;
					level[i].available = true;
					level[i + 1].available = true;
				}
			}

			if (quantityLevelsAvailable == 0)
			{
				level[0].color = WHITE;
				level[0].available = true;
			}

			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::changeCharacterPosX, pos::buttonsUp, (float)texture::changeCharacterButton.width, (float)texture::changeCharacterButton.height }))
			{
				changeCharacterButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::CHANGECHARACTER;
				}
			}
			else changeCharacterButton = WHITE;

			for (int i = 0; i < quantityLevels; i++)
			{
				if (CheckCollisionPointRec(mousePoint, { level[i].position.x, level[i].position.y, sizes::levelsSize, sizes::levelsSize }) && level[i].available &&
					(IsMouseButtonReleased(MOUSE_LEFT_BUTTON)))
				{
					sounds.button = true;
					actualLevel = i;
					level[actualLevel].actualMoves = 0;
					StopMusicStream(audio::gameMusic.menu);
					game::currentScene = game::Scene::GAMEPLAY;
					initialize();
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else backButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawTexture(texture::changeCharacterButton, pos::changeCharacterPosX, pos::buttonsUp, changeCharacterButton);

			for (int i = 0; i < quantityLevels; i++)
			{
				if (level[i].moves <= level[i].star3Moves && level[i].moves != 0)
				{
					level[i].star1 = WHITE;
					level[i].star2 = WHITE;
					level[i].star3 = WHITE;
				}
				else if (level[i].moves <= level[i].star2Moves && level[i].moves != 0)
				{
					level[i].star1 = WHITE;
					level[i].star2 = WHITE;
					level[i].star3 = DARKGRAY;
				}
				else if (level[i].moves <= level[i].star1Moves && level[i].moves != 0)
				{
					level[i].star1 = WHITE;
					level[i].star2 = DARKGRAY;
					level[i].star3 = DARKGRAY;
				}
				else
				{
					level[i].star1 = DARKGRAY;
					level[i].star2 = DARKGRAY;
					level[i].star3 = DARKGRAY;
				}

				DrawTexture(level[i].texture, level[i].position.x, level[i].position.y, level[i].color);
				DrawTexture(texture::star, level[i].position.x + (sizes::starsSize / 2), level[i].position.y + sizes::levelsSize, level[i].star1);
				DrawTexture(texture::star, level[i].position.x + sizes::starsSize * 2 - (sizes::starsSize / 2), level[i].position.y + sizes::levelsSize, level[i].star2);
				DrawTexture(texture::star, level[i].position.x + sizes::starsSize * 3 - (sizes::starsSize / 2), level[i].position.y + sizes::levelsSize, level[i].star3);

				if (i == 0) DrawText(TextFormat("%i", i + 1), level[i].position.x + pos::levelsPosX, level[i].position.y + pos::levelsPosY, sizes::textSize, level[i].color);
				else if (i == 9) DrawText(TextFormat("%i", i + 1), level[i].position.x + pos::levelsPosX2, level[i].position.y + pos::levelsPosY, sizes::textSize, level[i].color);
				else DrawText(TextFormat("%i", i + 1), level[i].position.x + pos::levelsPosX3, level[i].position.y + pos::levelsPosY, sizes::textSize, level[i].color);
			}

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{
			UnloadTexture(texture::level);
			UnloadTexture(texture::star);
			UnloadTexture(texture::changeCharacterButton);
		}
	}
}