#ifndef LEVELS_H
#define LEVELS_H

#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace levels
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif