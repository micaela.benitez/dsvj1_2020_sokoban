#include "game_audio.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace game_audio
	{
		static Color musicMute = WHITE;
		static Color musicLow = WHITE;
		static Color musicHigh = DARKGRAY;
		static Color soundsMute = WHITE;
		static Color soundsLow = WHITE;
		static Color soundsHigh = DARKGRAY;
		static Color backButton = WHITE;

		void init()
		{
			texture::audio1 = LoadTexture("res/raw/textures/audio1.png");
			texture::audio1.width = sizes::buttonsWidth;
			texture::audio1.height = sizes::buttonsHeight;

			texture::audio2 = LoadTexture("res/raw/textures/audio2.png");
			texture::audio2.width = sizes::buttonsWidth;
			texture::audio2.height = sizes::buttonsHeight;

			texture::audio3 = LoadTexture("res/raw/textures/audio3.png");
			texture::audio3.width = sizes::buttonsWidth;
			texture::audio3.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY2, (float)texture::audio1.width, (float)texture::audio1.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = DARKGRAY;
					musicLow = WHITE;
					musicHigh = WHITE;
					audio::musicVolume = 0.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY3, (float)texture::audio2.width, (float)texture::audio2.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = WHITE;
					musicLow = DARKGRAY;
					musicHigh = WHITE;
					audio::musicVolume = 0.5f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY4, (float)texture::audio3.width, (float)texture::audio3.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					musicMute = WHITE;
					musicLow = WHITE;
					musicHigh = DARKGRAY;
					audio::musicVolume = 1.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY6, (float)texture::audio1.width, (float)texture::audio1.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = DARKGRAY;
					soundsLow = WHITE;
					soundsHigh = WHITE;
					audio::soundVolume = 0.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY7, (float)texture::audio2.width, (float)texture::audio2.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = WHITE;
					soundsLow = DARKGRAY;
					soundsHigh = WHITE;
					audio::soundVolume = 2.0f;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::audioPosY8, (float)texture::audio3.width, (float)texture::audio3.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					soundsMute = WHITE;
					soundsLow = WHITE;
					soundsHigh = DARKGRAY;
					audio::soundVolume = 4.0f;
				}
			}

			if (game::previousScene == game::Scene::GAMEPLAY)
			{
				PlayMusicStream(audio::gameMusic.gameplay);
				audio::gameplayAudio();
			}
			else
			{
				PlayMusicStream(audio::gameMusic.menu);
				audio::menuAudio();
			}

			audio::setSoundsVolume();
			audio::setMusicVolume();

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
				}
			}
			else backButton = WHITE;

			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawText("Music", (screenWidth / 2) - MeasureText("Music", sizes::textSize5 / 2), pos::audioPosY, sizes::textSize5, BLACK);
			DrawTexture(texture::audio1, pos::middleButtonsPosX, pos::audioPosY2, musicMute);
			DrawTexture(texture::audio2, pos::middleButtonsPosX, pos::audioPosY3, musicLow);
			DrawTexture(texture::audio3, pos::middleButtonsPosX, pos::audioPosY4, musicHigh);

			DrawText("Sounds", (screenWidth / 2) - MeasureText("Sounds", sizes::textSize5 / 2), pos::audioPosY5, sizes::textSize5, BLACK);
			DrawTexture(texture::audio1, pos::middleButtonsPosX, pos::audioPosY6, soundsMute);
			DrawTexture(texture::audio2, pos::middleButtonsPosX, pos::audioPosY7, soundsLow);
			DrawTexture(texture::audio3, pos::middleButtonsPosX, pos::audioPosY8, soundsHigh);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{
			UnloadTexture(texture::audio1);
			UnloadTexture(texture::audio2);
			UnloadTexture(texture::audio3);
		}
	}
}