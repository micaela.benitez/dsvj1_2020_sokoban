#include "resolutions.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace resolutions
	{
		static Color resolution1ButtonStatus = DARKGRAY;
		static Color resolution2ButtonStatus = WHITE;
		static Color resolution3ButtonStatus = WHITE;
		static Color backButton = WHITE;

		void init()
		{
			texture::resolution1 = LoadTexture("res/raw/textures/resolution1.png");
			texture::resolution1.width = sizes::buttonsWidth;
			texture::resolution1.height = sizes::buttonsHeight;

			texture::resolution2 = LoadTexture("res/raw/textures/resolution2.png");
			texture::resolution2.width = sizes::buttonsWidth;
			texture::resolution2.height = sizes::buttonsHeight;

			texture::resolution3 = LoadTexture("res/raw/textures/resolution3.png");
			texture::resolution3.width = sizes::buttonsWidth;
			texture::resolution3.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY2, (float)texture::resolution1.width, (float)texture::resolution1.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = DARKGRAY;
					resolution2ButtonStatus = WHITE;
					resolution3ButtonStatus = WHITE;
					screenWidth = 1200;
					screenHeight = 900;
					changedResolution = true;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY3, (float)texture::resolution2.width, (float)texture::resolution2.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = WHITE;
					resolution2ButtonStatus = DARKGRAY;
					resolution3ButtonStatus = WHITE;
					screenWidth = 1100;
					screenHeight = 800;
					changedResolution = true;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::middleButtonsPosX, pos::resolutionPosY4, (float)texture::resolution3.width, (float)texture::resolution3.height }))
			{
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					resolution1ButtonStatus = WHITE;
					resolution2ButtonStatus = WHITE;
					resolution3ButtonStatus = DARKGRAY;
					screenWidth = 1000;
					screenHeight = 700;
					changedResolution = true;
				}
			}

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::SETTINGS;
				}
			}
			else backButton = WHITE;

			if (changedResolution)
			{
				SetWindowSize(screenWidth, screenHeight);
				sizes::updateSizes();
				texture::updateTextures();
				pos::updateVariables();
				gameplay::init();
				levels::init();
				if (game::previousScene == game::Scene::GAMEPLAY) entitiesLocation();
			}
			changedResolution = false;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawText("Resolution", (screenWidth / 2) - MeasureText("Resolution", sizes::textSize5 / 2), pos::resolutionPosY, sizes::textSize5, BLACK);
			DrawTexture(texture::resolution1, pos::middleButtonsPosX, pos::resolutionPosY2, resolution1ButtonStatus);
			DrawTexture(texture::resolution2, pos::middleButtonsPosX, pos::resolutionPosY3, resolution2ButtonStatus);
			DrawTexture(texture::resolution3, pos::middleButtonsPosX, pos::resolutionPosY4, resolution3ButtonStatus);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{
			UnloadTexture(texture::resolution1);
			UnloadTexture(texture::resolution2); 
			UnloadTexture(texture::resolution3);
		}
	}
}