#ifndef RESOLUTIONS_H
#define RESOLUTIONS_H

#include "scenes/gameplay/gameplay.h"

namespace sokoban
{
	namespace resolutions
	{
		void init();
		void update();
		void draw();
		void deinit();
	}
}

#endif