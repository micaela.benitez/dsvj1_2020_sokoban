#include "settings.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace settings
	{
		static Color resetButton = WHITE;
		static Color audioButton = WHITE;
		static Color resolutionsButton = WHITE;
		static Color backButton = WHITE;

		void init()
		{
			texture::audioButton = LoadTexture("res/raw/textures/audiobutton.png");
			texture::audioButton.width = sizes::buttonsWidth;
			texture::audioButton.height = sizes::buttonsHeight;

			texture::resolutionsButton = LoadTexture("res/raw/textures/resolutionsbutton.png");
			texture::resolutionsButton.width = sizes::buttonsWidth;
			texture::resolutionsButton.height = sizes::buttonsHeight;

			texture::resetScore = LoadTexture("res/raw/textures/resetscore.png");
			texture::resetScore.width = sizes::buttonsWidth;
			texture::resetScore.height = sizes::buttonsHeight;
		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX, pos::settingsPosY, (float)texture::resetScore.width, (float)texture::resetScore.height }))
			{
				resetButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					for (int i = 0; i < quantityLevels; i++)
					{
						sounds.button = true;
						level[i].moves = 100; 
						level[i].completed = false;
					}
					SaveScore();
				}
			}
			else resetButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX, pos::settingsPosY2, (float)texture::audioButton.width, (float)texture::audioButton.height }))
			{
				audioButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::AUDIO;
				}
			}
			else audioButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::settingsPosX2, pos::settingsPosY3, (float)texture::resolutionsButton.width, (float)texture::resolutionsButton.height }))
			{
				resolutionsButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::RESOLUTIONS;
				}
			}
			else resolutionsButton = WHITE;

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::previousScene;
				}
			}
			else backButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawTexture(texture::resetScore, pos::settingsPosX, pos::settingsPosY, resetButton);
			DrawTexture(texture::audioButton, pos::settingsPosX, pos::settingsPosY2, audioButton);
			DrawTexture(texture::resolutionsButton, pos::settingsPosX2, pos::settingsPosY3, resolutionsButton);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{
			UnloadTexture(texture::audioButton);
			UnloadTexture(texture::resolutionsButton);
			UnloadTexture(texture::resetScore);
		}
	}
}