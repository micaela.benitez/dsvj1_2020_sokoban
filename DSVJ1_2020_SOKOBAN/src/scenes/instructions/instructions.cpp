#include "instructions.h"

using namespace sokoban;
using namespace gameplay;

namespace sokoban
{
	namespace instructions
	{
		static Color backButton = WHITE;

		void init()
		{

		}

		void update()
		{
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, { pos::backButtonPosX, pos::backButtonPosY, (float)texture::backButton.width, (float)texture::backButton.height }))
			{
				backButton = DARKGRAY;
				if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					sounds.button = true;
					game::currentScene = game::Scene::MENU;
				}
			}
			else backButton = WHITE;

			PlayMusicStream(audio::gameMusic.menu);
			audio::menuAudio();
			sounds.button = false;
		}

		void draw()
		{
			DrawTexture(texture::background, 0, 0, WHITE);

			DrawText("In this game you have to help you character to carry all the boxes to", (screenWidth / 2) - MeasureText("In this game you have to help you character to carry all the boxes to", sizes::textSize4) / 2, pos::instructionsPosY, sizes::textSize4, WHITE);
			DrawText("the boxes to the white crosses. As you progress you will encounter", (screenWidth / 2) - MeasureText("the boxes to the white crosses. As you progress you will encounter", sizes::textSize4) / 2, pos::instructionsPosY2, sizes::textSize4, WHITE);
			DrawText("different obstacles:", (screenWidth / 2) - MeasureText("different obstacles:", sizes::textSize4) / 2, pos::instructionsPosY3, sizes::textSize4, WHITE);
			
			DrawText("Holes: if you fall into any of them you lose, but if you throw a box", (screenWidth / 2) - MeasureText("Holes: if you fall into any of them you lose, but if you throw a box", sizes::textSize4) / 2, pos::instructionsPosY4, sizes::textSize4, WHITE);
			DrawText("inside them, you will cover them and go over the top normally", (screenWidth / 2) - MeasureText("inside them, you will cover them and go over the top normally", sizes::textSize4) / 2, pos::instructionsPosY5, sizes::textSize4, WHITE);
			
			DrawText("Red doors: on the map you will find a red button, if you put a box on", (screenWidth / 2) - MeasureText("Red doors: on the map you will find a red button, if you put a box on", sizes::textSize4) / 2, pos::instructionsPosY6, sizes::textSize4, WHITE);
			DrawText("the top of it, the red door will open", (screenWidth / 2) - MeasureText("the top of it, the red door will open", sizes::textSize4) / 2, pos::instructionsPosY7, sizes::textSize4, WHITE);

			DrawText("Brown doors: on the map you will find a key, if you get it, the brown", (screenWidth / 2) - MeasureText("Brown doors: on the map you will find a key, if you get it, the brown", sizes::textSize4) / 2, pos::instructionsPosY8, sizes::textSize4, WHITE);
			DrawText("door will open", (screenWidth / 2) - MeasureText("door will open", sizes::textSize4) / 2, pos::instructionsPosY9, sizes::textSize4, WHITE);

			DrawText("Water: if you go over it, you will continue until there is no more", (screenWidth / 2) - MeasureText("Water: if you go over it, you will continue until there is no more", sizes::textSize4) / 2, pos::instructionsPosY10, sizes::textSize4, WHITE);
			DrawText("water, the same will happen if you place a box in top", (screenWidth / 2) - MeasureText("water, the same will happen if you place a box in top", sizes::textSize4) / 2, pos::instructionsPosY11, sizes::textSize4, WHITE);

			DrawTexture(texture::backButton, pos::backButtonPosX, pos::backButtonPosY, backButton);
		}

		void deinit()
		{

		}
	}
}