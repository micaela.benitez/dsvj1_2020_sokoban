#ifndef GAMEPLAY_H
#define GAMEPLAY_H

#include "raylib.h"

#include "functions_library/audio/audio.h"
#include "functions_library/audio/sounds.h"
#include "functions_library/drawings/drawings.h"
#include "functions_library/entities/entities.h"
#include "functions_library/entities_motion/entities_motion.h"
#include "functions_library/textures/textures.h"
#include "functions_library/sizes/sizes.h"
#include "functions_library/positions/positions.h"
#include "functions_library/condition_to_win/condition_to_win.h"
#include "functions_library/levels_generator/levels_generator.h"
#include "scenes/game/game.h"

const int blocksPerLine = 19;
const int blocksPerColumn = 17;

const int quantityLevels = 10;

const int maxQuantityBoxes = 50;
const int maxQuantityCrosses = 50;

namespace sokoban
{
	extern Color menuButton;
	extern Color levelsButton;
	extern Color settingsButton;
	extern Color replayLevelButton;

	extern Color winnerNextLevelButton;
	extern Color winnerRelayLevelButton;
	extern Color winnerMenuButton;
	extern Color winnerLevelsButton;

	namespace gameplay
	{
		extern int screenWidth;
		extern int screenHeight;

		extern Vector2 mousePoint;

		extern int quantityBlocksPerLevel;
		extern int maxLevel;

		extern Sounds sounds;

		extern Block block[blocksPerLine][blocksPerColumn];

		extern float timerCharacter;
		extern int frameCharacterWidth;
		extern int frameCharacterHeight;
		extern float frameWidth1;
		extern float frameHeight1;
		extern int maxFrames1;

		extern float timerStar;
		extern int frameStar;
		extern float frameWidth2;
		extern int maxFrames2;

		extern Character character;
		extern int auxMoves;

		extern int boxesAvailable;
		extern int crossesAvailable;
		extern Box box[maxQuantityBoxes];
		extern Box cross[maxQuantityCrosses];

		extern Key key;

		extern Level level[quantityLevels];

		extern int actualLevel;

		extern bool changedResolution;

		extern bool buttonPressed;
		extern Vector2 positionTile;
		extern Tiles doorPosition;

		extern bool pause;

		void init();
		void initialize();
		void update();
		void draw();
		void deinit();
	}
}

#endif