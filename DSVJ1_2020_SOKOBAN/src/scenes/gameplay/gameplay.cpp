#include "gameplay.h"

namespace sokoban
{
	Color menuButton = WHITE;
	Color levelsButton = WHITE;
	Color settingsButton = WHITE;
	Color replayLevelButton = WHITE;

	Color winnerNextLevelButton = WHITE;
	Color winnerRelayLevelButton = WHITE;
	Color winnerMenuButton = WHITE;
	Color winnerLevelsButton = WHITE;

	namespace gameplay
	{
		int screenWidth = 1200;
		int screenHeight = 900;

		Vector2 mousePoint = { 0.0f, 0.0f };

		int quantityBlocksPerLevel = 323;
		int maxLevel = quantityLevels - 1;

		Sounds sounds;

		Block block[blocksPerLine][blocksPerColumn];

		float timerCharacter = 0.0f;
		int frameCharacterWidth = 0;
		int frameCharacterHeight = 0;
		float frameWidth1 = 0.0f;
		float frameHeight1 = 0.0f;
		int maxFrames1 = 0;

		float timerStar = 0.0f;
		int frameStar = 0;
		float frameWidth2 = 0.0f;
		int maxFrames2 = 0;

		Character character;
		int auxMoves = 100;

		int boxesAvailable = 0;
		int crossesAvailable = 0;
		Box box[maxQuantityBoxes];
		Box cross[maxQuantityCrosses];

		Key key;

		Level level[quantityLevels];

		int actualLevel = 0;

		bool changedResolution = false;

		bool buttonPressed = false;
		Vector2 positionTile = { 0, 0 };
		Tiles doorPosition;

		bool pause = false;

		void init()
		{
			if (!changedResolution)
			{
				texture::gameBackground = LoadTexture("res/raw/textures/gamebackground.png");
				texture::gameBackground.width = screenWidth;
				texture::gameBackground.height = screenHeight;

				texture::gameBackgroundNight = LoadTexture("res/raw/textures/gamebackground2.png");
				texture::gameBackgroundNight.width = screenWidth;
				texture::gameBackgroundNight.height = screenHeight;

				texture::girl = LoadTexture("res/assets/textures/girl.png");
				texture::girl.width = sizes::entities * sizes::framesEntities;
				texture::girl.height = sizes::entities * sizes::framesEntities;

				texture::boy = LoadTexture("res/assets/textures/boy.png");
				texture::boy.width = sizes::entities * sizes::framesEntities;
				texture::boy.height = sizes::entities * sizes::framesEntities;

				texture::box = LoadTexture("res/assets/textures/box.png");
				texture::box.width = sizes::boxWidth;
				texture::box.height = sizes::boxHeight;

				texture::cross = LoadTexture("res/raw/textures/x.png");
				texture::cross.width = sizes::entities;
				texture::cross.height = sizes::entities;

				texture::grass = LoadTexture("res/assets/textures/grass.png");
				texture::grass.width = sizes::blocksWidth;
				texture::grass.height = sizes::blocksHeight;

				texture::wall = LoadTexture("res/raw/textures/wall.png");
				texture::wall.width = sizes::blocksWidth;
				texture::wall.height = sizes::blocksHeight;

				texture::floor = LoadTexture("res/raw/textures/floor.png");
				texture::floor.width = sizes::blocksWidth;
				texture::floor.height = sizes::blocksHeight;

				texture::hole = LoadTexture("res/raw/textures/floorhole.png");
				texture::hole.width = sizes::blocksWidth;
				texture::hole.height = sizes::blocksHeight;

				texture::redButton = LoadTexture("res/raw/textures/redbutton.png");
				texture::redButton.width = sizes::blocksWidth;
				texture::redButton.height = sizes::blocksHeight;

				texture::upRedDoor = LoadTexture("res/raw/textures/reddoor3.png");
				texture::upRedDoor.width = sizes::blocksWidth;
				texture::upRedDoor.height = sizes::blocksHeight;

				texture::downRedDoor = LoadTexture("res/raw/textures/reddoor1.png");
				texture::downRedDoor.width = sizes::blocksWidth;
				texture::downRedDoor.height = sizes::blocksHeight;

				texture::leftRedDoor = LoadTexture("res/raw/textures/reddoor4.png");
				texture::leftRedDoor.width = sizes::blocksWidth;
				texture::leftRedDoor.height = sizes::blocksHeight;

				texture::rightRedDoor = LoadTexture("res/raw/textures/reddoor2.png");
				texture::rightRedDoor.width = sizes::blocksWidth;
				texture::rightRedDoor.height = sizes::blocksHeight;

				texture::upBrownDoor = LoadTexture("res/raw/textures/browndoor3.png");
				texture::upBrownDoor.width = sizes::blocksWidth;
				texture::upBrownDoor.height = sizes::blocksHeight;

				texture::downBrownDoor = LoadTexture("res/raw/textures/browndoor1.png");
				texture::downBrownDoor.width = sizes::blocksWidth;
				texture::downBrownDoor.height = sizes::blocksHeight;

				texture::leftBrownDoor = LoadTexture("res/raw/textures/browndoor4.png");
				texture::leftBrownDoor.width = sizes::blocksWidth;
				texture::leftBrownDoor.height = sizes::blocksHeight;

				texture::rightBrownDoor = LoadTexture("res/raw/textures/browndoor2.png");
				texture::rightBrownDoor.width = sizes::blocksWidth;
				texture::rightBrownDoor.height = sizes::blocksHeight;

				texture::grassNight = LoadTexture("res/assets/textures/grass2.png");
				texture::grassNight.width = sizes::blocksWidth;
				texture::grassNight.height = sizes::blocksHeight;

				texture::wallNight = LoadTexture("res/raw/textures/wall2.png");
				texture::wallNight.width = sizes::blocksWidth;
				texture::wallNight.height = sizes::blocksHeight;

				texture::floorNight = LoadTexture("res/raw/textures/floor2.png");
				texture::floorNight.width = sizes::blocksWidth;
				texture::floorNight.height = sizes::blocksHeight;

				texture::holeNight = LoadTexture("res/raw/textures/floorhole2.png");
				texture::holeNight.width = sizes::blocksWidth;
				texture::holeNight.height = sizes::blocksHeight;

				texture::redButtonNight = LoadTexture("res/raw/textures/redbutton2.png");
				texture::redButtonNight.width = sizes::blocksWidth;
				texture::redButtonNight.height = sizes::blocksHeight;

				texture::upRedDoorNight = LoadTexture("res/raw/textures/reddoor32.png");
				texture::upRedDoorNight.width = sizes::blocksWidth;
				texture::upRedDoorNight.height = sizes::blocksHeight;

				texture::downRedDoorNight = LoadTexture("res/raw/textures/reddoor12.png");
				texture::downRedDoorNight.width = sizes::blocksWidth;
				texture::downRedDoorNight.height = sizes::blocksHeight;

				texture::leftRedDoorNight = LoadTexture("res/raw/textures/reddoor42.png");
				texture::leftRedDoorNight.width = sizes::blocksWidth;
				texture::leftRedDoorNight.height = sizes::blocksHeight;

				texture::rightRedDoorNight = LoadTexture("res/raw/textures/reddoor22.png");
				texture::rightRedDoorNight.width = sizes::blocksWidth;
				texture::rightRedDoorNight.height = sizes::blocksHeight;

				texture::upBrownDoorNight = LoadTexture("res/raw/textures/browndoor32.png");
				texture::upBrownDoorNight.width = sizes::blocksWidth;
				texture::upBrownDoorNight.height = sizes::blocksHeight;

				texture::downBrownDoorNight = LoadTexture("res/raw/textures/browndoor12.png");
				texture::downBrownDoorNight.width = sizes::blocksWidth;
				texture::downBrownDoorNight.height = sizes::blocksHeight;

				texture::leftBrownDoorNight = LoadTexture("res/raw/textures/browndoor42.png");
				texture::leftBrownDoorNight.width = sizes::blocksWidth;
				texture::leftBrownDoorNight.height = sizes::blocksHeight;

				texture::rightBrownDoorNight = LoadTexture("res/raw/textures/browndoor22.png");
				texture::rightBrownDoorNight.width = sizes::blocksWidth;
				texture::rightBrownDoorNight.height = sizes::blocksHeight;

				texture::water = LoadTexture("res/assets/textures/water.png");
				texture::water.width = sizes::blocksWidth;
				texture::water.height = sizes::blocksHeight;

				texture::key = LoadTexture("res/raw/textures/key.png");
				texture::key.width = sizes::blocksWidth;
				texture::key.height = sizes::blocksHeight;

				texture::menuButton = LoadTexture("res/raw/textures/menubutton.png");
				texture::menuButton.width = sizes::buttonsWidth;
				texture::menuButton.height = sizes::buttonsHeight;

				texture::levelsButton = LoadTexture("res/raw/textures/levelsbutton.png");
				texture::levelsButton.width = sizes::buttonsWidth;
				texture::levelsButton.height = sizes::buttonsHeight;

				texture::settingsButton2 = LoadTexture("res/raw/textures/settings.png");
				texture::settingsButton2.width = sizes::settingsButton;
				texture::settingsButton2.height = sizes::settingsButton;

				texture::menuButton2 = LoadTexture("res/raw/textures/menu.png");
				texture::menuButton2.width = sizes::settingsButton;
				texture::menuButton2.height = sizes::settingsButton;

				texture::levelsButton2 = LoadTexture("res/raw/textures/levels.png");
				texture::levelsButton2.width = sizes::settingsButton;
				texture::levelsButton2.height = sizes::settingsButton;

				texture::nextLevelButton = LoadTexture("res/raw/textures/nextlevel.png");
				texture::nextLevelButton.width = sizes::settingsButton;
				texture::nextLevelButton.height = sizes::settingsButton;

				texture::replayLevelButton = LoadTexture("res/raw/textures/replaylevel.png");
				texture::replayLevelButton.width = sizes::settingsButton;
				texture::replayLevelButton.height = sizes::settingsButton;

				texture::levelCompleteText = LoadTexture("res/raw/textures/levelcomplete.png");
				texture::levelCompleteText.width = screenWidth / 2.6f;
				texture::levelCompleteText.height = screenWidth / 7.25f;

				texture::levelCompleteSquare = LoadTexture("res/raw/textures/winnersquare.png");
				texture::levelCompleteSquare.width = screenWidth / 2.18f;
				texture::levelCompleteSquare.height = screenWidth / 2.18f;

				texture::loser = LoadTexture("res/raw/textures/loser.png");
				texture::loser.width = screenWidth / 2.6f;
				texture::loser.height = screenWidth / 7.25f;

				for (int i = 0; i < quantityLevels; i++)
				{
					level[i].completed = false;
					level[i].available = false;
					level[i].star1 = DARKGRAY;
					level[i].star2 = DARKGRAY;
					level[i].star3 = DARKGRAY;
					level[i].texture = texture::level;
					level[i].actualMoves = 0;
					level[i].moves = auxMoves;
				}

				character.texture = texture::girl;
			}

			for (int i = 0; i < blocksPerLine; i++)
			{
				for (int j = 0; j < blocksPerColumn; j++)
				{
					block[i][j].position = { i * sizes::blocksWidth, j * sizes::blocksHeight };
				}
			}

			//SaveScore();
			LoadScore();
		}

		void initialize()
		{
			loadLevels();
			starsPerLevels();

			textures();

			character.boxDown = false;
			character.boxLeft = false;
			character.boxUp = false;
			character.boxRight = false;
			character.lvlWon = false;
			character.loser = false;

			buttonPressed = false;
			positionTile = { 0, 0 };

			key.position = { 0, 0 };
			key.available = false;
			key.taken = false;

			entitiesLocation();

			pause = false;
		}

		void update()
		{
			if (IsKeyPressed(KEY_SPACE)) pause = (pause == false) ? true : false;   // Pause

			if (!pause)
			{
				framesEntities();
				checkEmptyBlocks();
				characterMotion();
				boxesMotion();
				checkEntitiesPosition();				

				conditionToWin();
				if (character.lvlWon || character.loser)
				{
					SaveScore();
					result();
					resultScreen();
				}
			}

			informationScreen();

			PlayMusicStream(audio::gameMusic.gameplay);
			audio::gameplayAudio();
			sounds.characterMoving = false;
			sounds.button = false;
			sounds.keyHole = false;
			sounds.redButton = false;
		}

		void draw()
		{
			if (actualLevel < 5) DrawTexture(texture::gameBackground, 0, 0, WHITE);
			else DrawTexture(texture::gameBackgroundNight, 0, 0, WHITE);

			if (!character.lvlWon && !character.loser) 
			{
				drawBlocks();
				drawCrosses();
				drawBoxes();
				drawKey();
				drawCharacter();
			}
			else drawResultScreen();

			drawInformationScreen();
		}

		void deinit()
		{
			UnloadTexture(texture::gameBackground);
			UnloadTexture(texture::gameBackgroundNight);
			UnloadTexture(texture::girl);
			UnloadTexture(texture::boy);
			UnloadTexture(texture::box);
			UnloadTexture(texture::cross);
			UnloadTexture(texture::grass);
			UnloadTexture(texture::wall);
			UnloadTexture(texture::floor);
			UnloadTexture(texture::hole);
			UnloadTexture(texture::redButton);
			UnloadTexture(texture::upRedDoor);
			UnloadTexture(texture::downRedDoor);
			UnloadTexture(texture::leftRedDoor);
			UnloadTexture(texture::rightRedDoor);
			UnloadTexture(texture::upBrownDoor);
			UnloadTexture(texture::downBrownDoor);
			UnloadTexture(texture::leftBrownDoor);
			UnloadTexture(texture::rightBrownDoor);
			UnloadTexture(texture::grassNight);
			UnloadTexture(texture::wallNight);
			UnloadTexture(texture::floorNight);
			UnloadTexture(texture::holeNight);
			UnloadTexture(texture::redButtonNight);
			UnloadTexture(texture::upRedDoorNight);
			UnloadTexture(texture::downRedDoorNight);
			UnloadTexture(texture::leftRedDoorNight);
			UnloadTexture(texture::rightRedDoorNight);
			UnloadTexture(texture::upBrownDoorNight);
			UnloadTexture(texture::downBrownDoorNight);
			UnloadTexture(texture::leftBrownDoorNight);
			UnloadTexture(texture::rightBrownDoorNight);
			UnloadTexture(texture::water);
			UnloadTexture(texture::key);
			UnloadTexture(texture::menuButton);
			UnloadTexture(texture::levelsButton);
			UnloadTexture(texture::settingsButton2);
			UnloadTexture(texture::menuButton2);
			UnloadTexture(texture::levelsButton2);
			UnloadTexture(texture::nextLevelButton);
			UnloadTexture(texture::replayLevelButton);
			UnloadTexture(texture::levelCompleteText);
			UnloadTexture(texture::levelCompleteSquare);
			UnloadTexture(texture::loser);
		}
	}
}