#ifndef TEXTURES_H
#define TEXTURES_H

#include "scenes/gameplay/gameplay.h"

namespace texture
{
	extern Texture2D background;
	extern Texture2D gameBackground;
	extern Texture2D gameBackgroundNight;
	extern Texture2D title;
	extern Texture2D titleCharacter;
	extern Texture2D girl;
	extern Texture2D boy;
	extern Texture2D girl2;
	extern Texture2D boy2;
	extern Texture2D box;
	extern Texture2D cross;
	extern Texture2D star;
	extern Texture2D levelCompleteText;
	extern Texture2D levelCompleteSquare;
	extern Texture2D loser;
	extern Texture2D key;

	// Buttons
	extern Texture2D playButton;
	extern Texture2D instructionsButton;
	extern Texture2D settingsButton;
	extern Texture2D settingsButton2;
	extern Texture2D creditsButton;
	extern Texture2D exitButton;
	extern Texture2D level;
	extern Texture2D changeCharacterButton;
	extern Texture2D backButton;
	extern Texture2D menuButton;
	extern Texture2D menuButton2;
	extern Texture2D levelsButton;
	extern Texture2D levelsButton2;
	extern Texture2D audioButton;
	extern Texture2D resolutionsButton;
	extern Texture2D nextLevelButton;
	extern Texture2D replayLevelButton;
	extern Texture2D audio1;
	extern Texture2D audio2;
	extern Texture2D audio3;
	extern Texture2D resolution1;
	extern Texture2D resolution2;
	extern Texture2D resolution3;
	extern Texture2D resetScore;

	// Blocks
	extern Texture2D grass;
	extern Texture2D wall;
	extern Texture2D floor;
	extern Texture2D hole;
	extern Texture2D redButton;
	extern Texture2D upRedDoor;
	extern Texture2D downRedDoor;
	extern Texture2D leftRedDoor;
	extern Texture2D rightRedDoor;
	extern Texture2D upBrownDoor;
	extern Texture2D downBrownDoor;
	extern Texture2D leftBrownDoor;
	extern Texture2D rightBrownDoor;
	extern Texture2D grassNight;
	extern Texture2D wallNight;
	extern Texture2D floorNight;
	extern Texture2D holeNight;
	extern Texture2D redButtonNight;
	extern Texture2D upRedDoorNight;
	extern Texture2D downRedDoorNight;
	extern Texture2D leftRedDoorNight;
	extern Texture2D rightRedDoorNight;
	extern Texture2D upBrownDoorNight;
	extern Texture2D downBrownDoorNight;
	extern Texture2D leftBrownDoorNight;
	extern Texture2D rightBrownDoorNight;
	extern Texture2D water;

	void updateTextures();
}

#endif