#include "textures.h"

using namespace sokoban;
using namespace gameplay;

namespace texture
{
	Texture2D background;
	Texture2D gameBackground;
	Texture2D gameBackgroundNight;
	Texture2D title;
	Texture2D titleCharacter;
	Texture2D girl;
	Texture2D boy;
	Texture2D girl2;
	Texture2D boy2;
	Texture2D box;
	Texture2D cross;
	Texture2D star;
	Texture2D levelCompleteText;
	Texture2D levelCompleteSquare;
	Texture2D loser;
	Texture2D key;

	// Buttons
	Texture2D playButton;
	Texture2D instructionsButton;
	Texture2D settingsButton;
	Texture2D settingsButton2;
	Texture2D creditsButton;
	Texture2D exitButton;
	Texture2D level;
	Texture2D changeCharacterButton;
	Texture2D backButton;
	Texture2D menuButton;
	Texture2D menuButton2;
	Texture2D levelsButton;
	Texture2D levelsButton2;
	Texture2D audioButton;
	Texture2D resolutionsButton;
	Texture2D nextLevelButton;
	Texture2D replayLevelButton;
	Texture2D audio1;
	Texture2D audio2;
	Texture2D audio3;
	Texture2D resolution1;
	Texture2D resolution2;
	Texture2D resolution3;
	Texture2D resetScore;

	// Blocks
	Texture2D grass;
	Texture2D wall;
	Texture2D floor;
	Texture2D hole;
	Texture2D redButton;
	Texture2D upRedDoor;
	Texture2D downRedDoor;
	Texture2D leftRedDoor;
	Texture2D rightRedDoor;
	Texture2D upBrownDoor;
	Texture2D downBrownDoor;
	Texture2D leftBrownDoor;
	Texture2D rightBrownDoor;
	Texture2D grassNight;
	Texture2D wallNight;
	Texture2D floorNight;
	Texture2D holeNight;
	Texture2D redButtonNight;
	Texture2D upRedDoorNight;
	Texture2D downRedDoorNight;
	Texture2D leftRedDoorNight;
	Texture2D rightRedDoorNight;
	Texture2D upBrownDoorNight;
	Texture2D downBrownDoorNight;
	Texture2D leftBrownDoorNight;
	Texture2D rightBrownDoorNight;
	Texture2D water;

	void updateTextures()
	{
		texture::background.width = screenWidth;
		texture::background.height = screenHeight;

		texture::gameBackground.width = screenWidth;
		texture::gameBackground.height = screenHeight;

		texture::gameBackgroundNight.width = screenWidth;
		texture::gameBackgroundNight.height = screenHeight;

		texture::girl.width = sizes::entities * sizes::framesEntities;
		texture::girl.height = sizes::entities * sizes::framesEntities;

		texture::boy.width = sizes::entities * sizes::framesEntities;
		texture::boy.height = sizes::entities * sizes::framesEntities;

		texture::box.width = sizes::boxWidth;
		texture::box.height = sizes::boxHeight;

		texture::cross.width = sizes::entities;
		texture::cross.height = sizes::entities;

		texture::grass.width = sizes::blocksWidth;
		texture::grass.height = sizes::blocksHeight;

		texture::wall.width = sizes::blocksWidth;
		texture::wall.height = sizes::blocksHeight;

		texture::floor.width = sizes::blocksWidth;
		texture::floor.height = sizes::blocksHeight;

		texture::hole.width = sizes::blocksWidth;
		texture::hole.height = sizes::blocksHeight;

		texture::redButton.width = sizes::blocksWidth;
		texture::redButton.height = sizes::blocksHeight;

		texture::upRedDoor.width = sizes::blocksWidth;
		texture::upRedDoor.height = sizes::blocksHeight;

		texture::downRedDoor.width = sizes::blocksWidth;
		texture::downRedDoor.height = sizes::blocksHeight;

		texture::leftRedDoor.width = sizes::blocksWidth;
		texture::leftRedDoor.height = sizes::blocksHeight;

		texture::rightRedDoor.width = sizes::blocksWidth;
		texture::rightRedDoor.height = sizes::blocksHeight;

		texture::upBrownDoor.width = sizes::blocksWidth;
		texture::upBrownDoor.height = sizes::blocksHeight;

		texture::downBrownDoor.width = sizes::blocksWidth;
		texture::downBrownDoor.height = sizes::blocksHeight;

		texture::leftBrownDoor.width = sizes::blocksWidth;
		texture::leftBrownDoor.height = sizes::blocksHeight;

		texture::rightBrownDoor.width = sizes::blocksWidth;
		texture::rightBrownDoor.height = sizes::blocksHeight;

		texture::grassNight.width = sizes::blocksWidth;
		texture::grassNight.height = sizes::blocksHeight;

		texture::wallNight.width = sizes::blocksWidth;
		texture::wallNight.height = sizes::blocksHeight;

		texture::floorNight.width = sizes::blocksWidth;
		texture::floorNight.height = sizes::blocksHeight;

		texture::holeNight.width = sizes::blocksWidth;
		texture::holeNight.height = sizes::blocksHeight;

		texture::redButtonNight.width = sizes::blocksWidth;
		texture::redButtonNight.height = sizes::blocksHeight;

		texture::upRedDoorNight.width = sizes::blocksWidth;
		texture::upRedDoorNight.height = sizes::blocksHeight;

		texture::downRedDoorNight.width = sizes::blocksWidth;
		texture::downRedDoorNight.height = sizes::blocksHeight;

		texture::leftRedDoorNight.width = sizes::blocksWidth;
		texture::leftRedDoorNight.height = sizes::blocksHeight;

		texture::rightRedDoorNight.width = sizes::blocksWidth;
		texture::rightRedDoorNight.height = sizes::blocksHeight;

		texture::upBrownDoorNight.width = sizes::blocksWidth;
		texture::upBrownDoorNight.height = sizes::blocksHeight;

		texture::downBrownDoorNight.width = sizes::blocksWidth;
		texture::downBrownDoorNight.height = sizes::blocksHeight;

		texture::leftBrownDoorNight.width = sizes::blocksWidth;
		texture::leftBrownDoorNight.height = sizes::blocksHeight;

		texture::rightBrownDoorNight.width = sizes::blocksWidth;
		texture::rightBrownDoorNight.height = sizes::blocksHeight;

		texture::water.width = sizes::blocksWidth;
		texture::water.height = sizes::blocksHeight;

		texture::key.width = sizes::blocksWidth;
		texture::key.height = sizes::blocksHeight;

		texture::title.width = sizes::titleWidth;
		texture::title.height = sizes::titleHeight;

		texture::playButton.width = sizes::buttonsWidth;
		texture::playButton.height = sizes::buttonsHeight;

		texture::instructionsButton.width = sizes::buttonsWidth;
		texture::instructionsButton.height = sizes::buttonsHeight;

		texture::settingsButton.width = sizes::buttonsWidth;
		texture::settingsButton.height = sizes::buttonsHeight;

		texture::creditsButton.width = sizes::buttonsWidth;
		texture::creditsButton.height = sizes::buttonsHeight;

		texture::exitButton.width = sizes::buttonsWidth;
		texture::exitButton.height = sizes::buttonsHeight;

		texture::changeCharacterButton.width = sizes::buttonsWidth;
		texture::changeCharacterButton.height = sizes::buttonsHeight;

		texture::backButton.width = sizes::buttonsWidth;
		texture::backButton.height = sizes::buttonsHeight;

		texture::menuButton.width = sizes::buttonsWidth;
		texture::menuButton.height = sizes::buttonsHeight;

		texture::levelsButton.width = sizes::buttonsWidth;
		texture::levelsButton.height = sizes::buttonsHeight;

		texture::audioButton.width = sizes::buttonsWidth;
		texture::audioButton.height = sizes::buttonsHeight;

		texture::resolutionsButton.width = sizes::buttonsWidth;
		texture::resolutionsButton.height = sizes::buttonsHeight;

		texture::audio1.width = sizes::buttonsWidth;
		texture::audio1.height = sizes::buttonsHeight;

		texture::audio2.width = sizes::buttonsWidth;
		texture::audio2.height = sizes::buttonsHeight;

		texture::audio3.width = sizes::buttonsWidth;
		texture::audio3.height = sizes::buttonsHeight;

		texture::resolution1.width = sizes::buttonsWidth;
		texture::resolution1.height = sizes::buttonsHeight;

		texture::resolution2.width = sizes::buttonsWidth;
		texture::resolution2.height = sizes::buttonsHeight;

		texture::resolution3.width = sizes::buttonsWidth;
		texture::resolution3.height = sizes::buttonsHeight;

		texture::resetScore.width = sizes::buttonsWidth;
		texture::resetScore.height = sizes::buttonsHeight;

		texture::level.width = sizes::levelsSize;
		texture::level.height = sizes::levelsSize;

		texture::star.width = sizes::starsSize;
		texture::star.height = sizes::starsSize;

		texture::girl2.width = sizes::entitiesWidth;
		texture::girl2.height = sizes::entitiesHeight;

		texture::boy2.width = sizes::entitiesWidth;
		texture::boy2.height = sizes::entitiesHeight;

		texture::titleCharacter.width = sizes::titleEntitiesWidth;
		texture::titleCharacter.height = sizes::titleEntitiesHeight;

		texture::settingsButton2.width = sizes::settingsButton;
		texture::settingsButton2.height = sizes::settingsButton;

		texture::menuButton2.width = sizes::settingsButton;
		texture::menuButton2.height = sizes::settingsButton;

		texture::levelsButton2.width = sizes::settingsButton;
		texture::levelsButton2.height = sizes::settingsButton;

		texture::nextLevelButton.width = sizes::settingsButton;
		texture::nextLevelButton.height = sizes::settingsButton;

		texture::replayLevelButton.width = sizes::settingsButton;
		texture::replayLevelButton.height = sizes::settingsButton;

		texture::levelCompleteText.width = screenWidth / 2.6f;
		texture::levelCompleteText.height = screenWidth / 7.25f;

		texture::loser.width = screenWidth / 2.6f;
		texture::loser.height = screenWidth / 7.25f;

		texture::levelCompleteSquare.width = screenWidth / 2.18f;
		texture::levelCompleteSquare.height = screenWidth / 2.18f;
	}
}