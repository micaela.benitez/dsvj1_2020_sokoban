#ifndef DRAWINGS_H
#define DRAWINGS_H

#include "scenes/gameplay/gameplay.h"

void drawBlocks();
void drawCrosses();
void drawBoxes();
void drawKey();
void drawCharacter();
void drawInformationScreen();
void drawResultScreen();

#endif