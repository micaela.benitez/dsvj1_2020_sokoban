#include "drawings.h"

using namespace sokoban;
using namespace gameplay;

void drawBlocks()
{
	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++) DrawTexture(block[i][j].texture, block[i][j].position.x, block[i][j].position.y, WHITE);
	}

	#if DEBUG
	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++) DrawRectangleLines(block[i][j].position.x, block[i][j].position.y, sizes::blocksWidth, sizes::blocksHeight, BLACK);
	}
	#endif
}

void drawCrosses()
{
	for (int i = 0; i < crossesAvailable; i++) DrawTexture(texture::cross, cross[i].position.x, cross[i].position.y, WHITE);

	#if DEBUG
	for (int i = 0; i < crossesAvailable; i++) DrawRectangleLines(cross[i].position.x, cross[i].position.y, sizes::entities, sizes::entities, WHITE);
	#endif
}

void drawBoxes()
{
	for (int i = 0; i < boxesAvailable; i++)
	{
		if (box[i].available) DrawTexture(texture::box, box[i].position.x, box[i].position.y, WHITE);
	}		

	#if DEBUG
	for (int i = 0; i < boxesAvailable; i++)
	{
		if (box[i].available) DrawRectangleLines(box[i].position.x, box[i].position.y, sizes::boxWidth, sizes::boxHeight, WHITE);
	}
	#endif
}

void drawKey()
{
	if (key.available) DrawTexture(texture::key, key.position.x, key.position.y, WHITE);

	#if DEBUG
	if (key.available) DrawRectangleLines(key.position.x, key.position.y, sizes::blocksWidth, sizes::blocksHeight, WHITE);
	#endif
}

void drawCharacter()
{
	if (screenWidth == 1200) DrawTextureRec(character.texture, { frameWidth1 * frameCharacterWidth, frameHeight1 * frameCharacterHeight, frameWidth1, frameHeight1 }, { character.position.x + (screenWidth / 200), character.position.y }, WHITE);
	else if (screenWidth == 1100) DrawTextureRec(character.texture, { frameWidth1 * frameCharacterWidth, frameHeight1 * frameCharacterHeight, frameWidth1, frameHeight1 }, { character.position.x + (screenWidth / 200), character.position.y - (screenWidth / 300) }, WHITE);
	else if (screenWidth == 1000) DrawTextureRec(character.texture, { frameWidth1 * frameCharacterWidth, frameHeight1 * frameCharacterHeight, frameWidth1, frameHeight1 }, { character.position.x + (screenWidth / 400), character.position.y - (screenWidth / 200) }, WHITE);

	#if DEBUG
	if (screenWidth == 1200) DrawRectangleLines(character.position.x + (screenWidth / 200), character.position.y, sizes::entities, sizes::entities, WHITE);
	else if (screenWidth == 1100) DrawRectangleLines(character.position.x + (screenWidth / 200), character.position.y - (screenWidth / 300), sizes::entities, sizes::entities, WHITE);
	else if (screenWidth == 1000) DrawRectangleLines(character.position.x + (screenWidth / 400), character.position.y - (screenWidth / 200), sizes::entities, sizes::entities, WHITE);
	#endif
}

void drawInformationScreen()
{
	DrawTexture(texture::star, pos::starsUp, pos::starsUp, level[actualLevel].star1);
	DrawTexture(texture::star, pos::starsUp + sizes::starsSize, pos::starsUp, level[actualLevel].star2);
	DrawTexture(texture::star, pos::starsUp + sizes::starsSize * 2, pos::starsUp, level[actualLevel].star3);

	DrawText(TextFormat("%i moves", level[actualLevel].star1Moves), pos::informationUp, screenWidth / 120, sizes::textSize2, level[actualLevel].star1);
	DrawText(TextFormat("%i moves", level[actualLevel].star2Moves), pos::informationUp, (screenWidth / 120) + sizes::textSize2, sizes::textSize2, level[actualLevel].star2);
	DrawText(TextFormat("%i moves", level[actualLevel].star3Moves), pos::informationUp, (screenWidth / 120) + sizes::textSize2 * 2, sizes::textSize2, level[actualLevel].star3);

	if (!character.lvlWon)
	{
		DrawTexture(texture::menuButton, pos::gamePosX, pos::gamePosY, menuButton);
		DrawTexture(texture::levelsButton, pos::gamePosX2, pos::gamePosY, levelsButton);
		DrawTexture(texture::settingsButton2, pos::gamePosX3, pos::gamePosY2, settingsButton);
		DrawTexture(texture::replayLevelButton, pos::gamePosX3, pos::gamePosY3, replayLevelButton);
	}

	if (pause) DrawText("Pause", pos::pausePosX, pos::pausePosY, sizes::textSize5, WHITE);
}

void drawResultScreen()
{
	DrawTexture(texture::levelCompleteSquare, pos::winnerScreenPosX, pos::winnerScreenPosY, WHITE);
	if (!character.loser) DrawTexture(texture::levelCompleteText, pos::winnerScreenPosX2, pos::winnerScreenPosY2, WHITE);
	else
	{
		DrawTexture(texture::loser, pos::winnerScreenPosX2, pos::winnerScreenPosY2, WHITE);
		winnerNextLevelButton = DARKGRAY;
	}
	DrawTexture(texture::nextLevelButton, pos::winnerScreenPosX3, pos::winnerScreenPosY3, winnerNextLevelButton);
	DrawTexture(texture::replayLevelButton, pos::winnerScreenPosX4, pos::winnerScreenPosY4, winnerRelayLevelButton);
	DrawTexture(texture::menuButton2, pos::winnerScreenPosX5, pos::winnerScreenPosY5, winnerMenuButton);
	DrawTexture(texture::levelsButton2, pos::winnerScreenPosX6, pos::winnerScreenPosY6, winnerLevelsButton);
}