#include "entities_motion.h"

using namespace sokoban;
using namespace gameplay;

void framesEntities()
{
	frameWidth1 = sizes::entities;
	if (IsKeyPressed(KEY_DOWN)) frameCharacterWidth = 0;
	else if (IsKeyPressed(KEY_LEFT)) frameCharacterWidth = 1;
	else if (IsKeyPressed(KEY_UP)) frameCharacterWidth = 2;
	else if (IsKeyPressed(KEY_RIGHT)) frameCharacterWidth = 3;

	frameHeight1 = sizes::entities;
	maxFrames1 = (int)(texture::girl.height / (int)frameHeight1);
	timerCharacter += GetFrameTime();
	if (timerCharacter >= 0.1f)
	{
		timerCharacter = 0.0f;
		frameCharacterHeight++;
	}
	if (IsKeyDown(KEY_DOWN) || IsKeyDown(KEY_LEFT) || IsKeyDown(KEY_UP) || IsKeyDown(KEY_RIGHT)) frameCharacterHeight = frameCharacterHeight % maxFrames1;
	else frameCharacterHeight = 0;
}

void characterMotion()
{
	character.boxDown = false;
	character.boxLeft = false;
	character.boxUp = false;
	character.boxRight = false;

	for (int i = 0; i < boxesAvailable; i++)
	{
		if (box[i].position.x == character.position.x && box[i].position.y == character.position.y + sizes::blocksHeight && box[i].available) character.boxDown = true;
		else if (box[i].position.x == character.position.x - sizes::blocksWidth && box[i].position.y == character.position.y && box[i].available) character.boxLeft = true;
		else if (box[i].position.x == character.position.x && box[i].position.y == character.position.y - sizes::blocksHeight && box[i].available) character.boxUp = true;
		else if (box[i].position.x == character.position.x + sizes::blocksWidth && box[i].position.y == character.position.y && box[i].available) character.boxRight = true;
	}

	if (IsKeyPressed(KEY_DOWN) || (block[(int)character.posBlock.x][(int)character.posBlock.y].tile == Tiles::WATER && frameCharacterWidth == 0))
	{
		if ((block[(int)character.posBlock.x][(int)character.posBlock.y + 1].tile == Tiles::FLOOR || block[(int)character.posBlock.x][(int)character.posBlock.y + 1].tile == Tiles::HOLE
			|| block[(int)character.posBlock.x][(int)character.posBlock.y + 1].tile == Tiles::BUTTON || block[(int)character.posBlock.x][(int)character.posBlock.y + 1].tile == Tiles::WATER)
			&& !(character.boxDown && ((block[(int)character.posBlock.x][(int)character.posBlock.y + 2].tile != Tiles::FLOOR && 
			block[(int)character.posBlock.x][(int)character.posBlock.y + 2].tile != Tiles::HOLE && block[(int)character.posBlock.x][(int)character.posBlock.y + 2].tile != Tiles::BUTTON &&
			block[(int)character.posBlock.x][(int)character.posBlock.y + 2].tile != Tiles::WATER) || !block[(int)character.posBlock.x][(int)character.posBlock.y + 2].empty)))
		{
			sounds.characterMoving = true;
			level[actualLevel].actualMoves++;
			character.posBlock.y++;
		}
	}
	else if (IsKeyPressed(KEY_LEFT) || (block[(int)character.posBlock.x][(int)character.posBlock.y].tile == Tiles::WATER && frameCharacterWidth == 1))
	{
		if ((block[(int)character.posBlock.x - 1][(int)character.posBlock.y].tile == Tiles::FLOOR || block[(int)character.posBlock.x - 1][(int)character.posBlock.y].tile == Tiles::HOLE
			|| block[(int)character.posBlock.x - 1][(int)character.posBlock.y].tile == Tiles::BUTTON || block[(int)character.posBlock.x - 1][(int)character.posBlock.y].tile == Tiles::WATER)
			&& !(character.boxLeft && ((block[(int)character.posBlock.x - 2][(int)character.posBlock.y].tile != Tiles::FLOOR &&
			block[(int)character.posBlock.x - 2][(int)character.posBlock.y].tile != Tiles::HOLE && block[(int)character.posBlock.x - 2][(int)character.posBlock.y].tile != Tiles::BUTTON &&
			block[(int)character.posBlock.x - 2][(int)character.posBlock.y].tile != Tiles::WATER) || !block[(int)character.posBlock.x - 2][(int)character.posBlock.y].empty)))
		{
			sounds.characterMoving = true;
			level[actualLevel].actualMoves++;
			character.posBlock.x--;
		}
	}
	else if (IsKeyPressed(KEY_UP) || (block[(int)character.posBlock.x][(int)character.posBlock.y].tile == Tiles::WATER && frameCharacterWidth == 2))
	{
		if ((block[(int)character.posBlock.x][(int)character.posBlock.y - 1].tile == Tiles::FLOOR || block[(int)character.posBlock.x][(int)character.posBlock.y - 1].tile == Tiles::HOLE
			|| block[(int)character.posBlock.x][(int)character.posBlock.y - 1].tile == Tiles::BUTTON || block[(int)character.posBlock.x][(int)character.posBlock.y - 1].tile == Tiles::WATER)
			&& !(character.boxUp && ((block[(int)character.posBlock.x][(int)character.posBlock.y - 2].tile != Tiles::FLOOR &&
			block[(int)character.posBlock.x][(int)character.posBlock.y - 2].tile != Tiles::HOLE && block[(int)character.posBlock.x][(int)character.posBlock.y - 2].tile != Tiles::BUTTON &&
			block[(int)character.posBlock.x][(int)character.posBlock.y - 2].tile != Tiles::WATER) || !block[(int)character.posBlock.x][(int)character.posBlock.y - 2].empty)))
		{
			sounds.characterMoving = true;
			level[actualLevel].actualMoves++;
			character.posBlock.y--;
		}
	}
	else if (IsKeyPressed(KEY_RIGHT) || (block[(int)character.posBlock.x][(int)character.posBlock.y].tile == Tiles::WATER && frameCharacterWidth == 3))
	{
		if ((block[(int)character.posBlock.x + 1][(int)character.posBlock.y].tile == Tiles::FLOOR || block[(int)character.posBlock.x + 1][(int)character.posBlock.y].tile == Tiles::HOLE
			|| block[(int)character.posBlock.x + 1][(int)character.posBlock.y].tile == Tiles::BUTTON || block[(int)character.posBlock.x + 1][(int)character.posBlock.y].tile == Tiles::WATER)
			&& !(character.boxRight && ((block[(int)character.posBlock.x + 2][(int)character.posBlock.y].tile != Tiles::FLOOR &&
			block[(int)character.posBlock.x + 2][(int)character.posBlock.y].tile != Tiles::HOLE && block[(int)character.posBlock.x + 2][(int)character.posBlock.y].tile != Tiles::BUTTON &&
			block[(int)character.posBlock.x + 2][(int)character.posBlock.y].tile != Tiles::WATER) || !block[(int)character.posBlock.x + 2][(int)character.posBlock.y].empty)))
		{
			sounds.characterMoving = true;
			level[actualLevel].actualMoves++;
			character.posBlock.x++;
		}
	}

	character.position = block[(int)character.posBlock.x][(int)character.posBlock.y].position;
}

void boxesMotion()
{
	int auxPosBlock = 0;

	for (int i = 0; i < boxesAvailable; i++)
	{
		if (box[i].available)
		{
			if (character.position.y == box[i].position.y && frameCharacterWidth == 0 && character.position.x >= box[i].position.x && character.position.x <= box[i].position.x)
			{
				auxPosBlock = character.posBlock.y + 1;
				box[i].position = block[(int)character.posBlock.x][(int)auxPosBlock].position;
				if (block[(int)character.posBlock.x][(int)auxPosBlock].tile == Tiles::WATER && block[(int)character.posBlock.x][(int)auxPosBlock + 1].empty)
				{
					while (block[(int)character.posBlock.x][(int)auxPosBlock].tile == Tiles::WATER)
					{
						auxPosBlock++;
						box[i].position = block[(int)character.posBlock.x][(int)auxPosBlock].position;
					}
				}
			}
			else if (character.position.x == box[i].position.x && frameCharacterWidth == 1 && character.position.y >= box[i].position.y && character.position.y <= box[i].position.y)
			{
				auxPosBlock = character.posBlock.x - 1;
				box[i].position = block[(int)auxPosBlock][(int)character.posBlock.y].position;
				if (block[(int)auxPosBlock][(int)character.posBlock.y].tile == Tiles::WATER && block[(int)auxPosBlock - 1][(int)character.posBlock.y].empty)
				{
					while (block[(int)auxPosBlock][(int)character.posBlock.y].tile == Tiles::WATER)
					{
						auxPosBlock--;
						box[i].position = block[(int)auxPosBlock][(int)character.posBlock.y].position;
					}
				}
			}
			else if (character.position.y == box[i].position.y && frameCharacterWidth == 2 && character.position.x >= box[i].position.x && character.position.x <= box[i].position.x)
			{
				auxPosBlock = character.posBlock.y - 1;
				box[i].position = block[(int)character.posBlock.x][(int)auxPosBlock].position;
				if (block[(int)character.posBlock.x][(int)auxPosBlock].tile == Tiles::WATER && block[(int)character.posBlock.x][(int)auxPosBlock - 1].empty)
				{
					while (block[(int)character.posBlock.x][(int)auxPosBlock].tile == Tiles::WATER)
					{
						auxPosBlock--;
						box[i].position = block[(int)character.posBlock.x][(int)auxPosBlock].position;
					}
				}
			}
			else if (character.position.x == box[i].position.x && frameCharacterWidth == 3 && character.position.y >= box[i].position.y && character.position.y <= box[i].position.y)
			{
				auxPosBlock = character.posBlock.x + 1;
				box[i].position = block[(int)auxPosBlock][(int)character.posBlock.y].position;
				if (block[(int)auxPosBlock][(int)character.posBlock.y].tile == Tiles::WATER && block[(int)auxPosBlock + 1][(int)character.posBlock.y].empty)
				{
					while (block[(int)auxPosBlock][(int)character.posBlock.y].tile == Tiles::WATER)
					{
						auxPosBlock++;
						box[i].position = block[(int)auxPosBlock][(int)character.posBlock.y].position;
					}
				}
			}
		}
	}
}

void checkEntitiesPosition()
{
	if (key.available)
	{
		if (character.position.x == key.position.x && character.position.y == key.position.y)
		{
			sounds.keyHole = false;
			key.taken = true;
			key.available = false;
		}
	}

	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			if (block[i][j].tile == Tiles::HOLE)
			{
				if (character.position.x == block[i][j].position.x && character.position.y == block[i][j].position.y)
				{
					character.loser = true;
					level[actualLevel].actualMoves = auxMoves;
				}

				for (int r = 0; r < boxesAvailable; r++)
				{
					if (box[r].position.x == block[i][j].position.x && box[r].position.y == block[i][j].position.y)
					{
						sounds.keyHole = true;
						box[r].available = false;
						block[i][j].tile = Tiles::FLOOR;
						if (actualLevel < 5) block[i][j].texture = texture::floor;
						else block[i][j].texture = texture::floorNight;
					}
				}
			}
			else if (block[i][j].tile == Tiles::BUTTON)
			{
				for (int r = 0; r < boxesAvailable; r++)
				{
					if (box[r].position.x == block[i][j].position.x && box[r].position.y == block[i][j].position.y ||
						character.position.x == block[i][j].position.x && character.position.y == block[i][j].position.y)
					{
						sounds.redButton = true;
						buttonPressed = true;
						r = boxesAvailable;
					}
					else buttonPressed = false;
				}
			}
		}
	}

	if (buttonPressed)
	{
		for (int i = 0; i < blocksPerLine; i++)
		{
			for (int j = 0; j < blocksPerColumn; j++)
			{
				if (block[i][j].tile == Tiles::UPREDDOOR || block[i][j].tile == Tiles::DOWNREDDOOR || block[i][j].tile == Tiles::LEFTREDDOOR || block[i][j].tile == Tiles::RIGHTREDDOOR)
				{
					positionTile = { (float)i, (float)j };
					doorPosition = block[i][j].tile;
					block[i][j].tile = Tiles::FLOOR;
					if (actualLevel < 5) block[i][j].texture = texture::floor;
					else block[i][j].texture = texture::floorNight;
				}
			}
		}
	}
	else
	{
		block[(int)positionTile.x][(int)positionTile.y].tile = doorPosition;
		if (doorPosition == Tiles::UPREDDOOR) block[(int)positionTile.x][(int)positionTile.y].texture = texture::upRedDoor;
		else if (doorPosition == Tiles::DOWNREDDOOR) block[(int)positionTile.x][(int)positionTile.y].texture = texture::downRedDoor;
		else if (doorPosition == Tiles::LEFTREDDOOR) block[(int)positionTile.x][(int)positionTile.y].texture = texture::leftRedDoor;
		else if (doorPosition == Tiles::RIGHTREDDOOR) block[(int)positionTile.x][(int)positionTile.y].texture = texture::rightRedDoor;
	}

	if (key.taken)
	{
		for (int i = 0; i < blocksPerLine; i++)
		{
			for (int j = 0; j < blocksPerColumn; j++)
			{
				if (block[i][j].tile == Tiles::UPBROWNDOOR || block[i][j].tile == Tiles::DOWNBROWNDOOR || block[i][j].tile == Tiles::LEFTBROWNDOOR || block[i][j].tile == Tiles::RIGHTBROWNDOOR)
				{
					block[i][j].tile = Tiles::FLOOR;
					if (actualLevel < 5) block[i][j].texture = texture::floor;
					else block[i][j].texture = texture::floorNight;
				}
			}
		}
	}
}

void informationScreen()
{
	if (level[actualLevel].actualMoves <= level[actualLevel].star3Moves)
	{
		level[actualLevel].star1 = WHITE;
		level[actualLevel].star2 = WHITE;
		level[actualLevel].star3 = WHITE;
	}
	else if (level[actualLevel].actualMoves <= level[actualLevel].star2Moves) level[actualLevel].star3 = DARKGRAY;
	else if (level[actualLevel].actualMoves <= level[actualLevel].star1Moves) level[actualLevel].star2 = DARKGRAY;
	else level[actualLevel].star1 = DARKGRAY;

	mousePoint = GetMousePosition();

	if (CheckCollisionPointRec(mousePoint, { pos::gamePosX, pos::gamePosY, (float)texture::menuButton.width, (float)texture::menuButton.height }))
	{
		menuButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			StopMusicStream(audio::gameMusic.gameplay);
			game::currentScene = game::Scene::MENU;
		}
	}
	else menuButton = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::gamePosX2, pos::gamePosY, (float)texture::levelsButton.width, (float)texture::levelsButton.height }))
	{
		levelsButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			StopMusicStream(audio::gameMusic.gameplay);
			game::currentScene = game::Scene::LEVELS;
		}
	}
	else levelsButton = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::gamePosX3, pos::gamePosY2, (float)texture::settingsButton2.width, (float)texture::settingsButton2.height }))
	{
		settingsButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			StopMusicStream(audio::gameMusic.gameplay);
			game::currentScene = game::Scene::SETTINGS;
			game::previousScene = game::Scene::GAMEPLAY;
		}
	}
	else settingsButton = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::gamePosX3, pos::gamePosY3, (float)texture::replayLevelButton.width, (float)texture::replayLevelButton.height }))
	{
		replayLevelButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			level[actualLevel].actualMoves = 0;
			initialize();
		}
	}
	else replayLevelButton = WHITE;
}

void resultScreen()
{
	mousePoint = GetMousePosition();

	if (!character.loser)
	{
		if (CheckCollisionPointRec(mousePoint, { pos::winnerScreenPosX3, pos::winnerScreenPosY3, (float)texture::nextLevelButton.width, (float)texture::nextLevelButton.height }))
		{
			winnerNextLevelButton = DARKGRAY;
			if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				sounds.button = true;
				initialize();
				actualLevel++;
				level[actualLevel].actualMoves = 0;
				initialize();
			}
		}
		else winnerNextLevelButton = WHITE;
	}

	if (CheckCollisionPointRec(mousePoint, { pos::winnerScreenPosX4, pos::winnerScreenPosY4, (float)texture::replayLevelButton.width, (float)texture::replayLevelButton.height }))
	{
		winnerRelayLevelButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			level[actualLevel].actualMoves = 0;
			initialize();
		}
	}
	else winnerRelayLevelButton = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::winnerScreenPosX5, pos::winnerScreenPosY5, (float)texture::menuButton2.width, (float)texture::menuButton2.height }))
	{
		winnerMenuButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			StopMusicStream(audio::gameMusic.gameplay);
			game::currentScene = game::Scene::MENU;
		}
	}
	else winnerMenuButton = WHITE;

	if (CheckCollisionPointRec(mousePoint, { pos::winnerScreenPosX6, pos::winnerScreenPosY6, (float)texture::levelsButton2.width, (float)texture::levelsButton2.height }))
	{
		winnerLevelsButton = DARKGRAY;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			sounds.button = true;
			StopMusicStream(audio::gameMusic.gameplay);
			game::currentScene = game::Scene::LEVELS;
		}
	}
	else winnerLevelsButton = WHITE;
}

void checkEmptyBlocks() 
{
	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			block[i][j].empty = true;
		}
	}

	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			for (int r = 0; r < boxesAvailable; r++)
			{
				if (block[i][j].position.x == box[r].position.x && block[i][j].position.y == box[r].position.y && box[r].available)
				{
					block[i][j].empty = false;
				}
			}
		}
	}
}