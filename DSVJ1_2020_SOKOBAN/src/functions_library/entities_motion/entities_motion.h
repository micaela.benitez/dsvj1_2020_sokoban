#ifndef ENTITIES_MOTION_H
#define ENTITIES_MOTION_H

#include "scenes/gameplay/gameplay.h"

void framesEntities();
void characterMotion();
void boxesMotion();
void checkEntitiesPosition();
void informationScreen();
void resultScreen();
void checkEmptyBlocks();

#endif