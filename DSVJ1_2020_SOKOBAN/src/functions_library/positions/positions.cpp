
#include "positions.h"

using namespace sokoban;
using namespace gameplay;

namespace pos
{
	float titlePosX = (screenWidth / 2) - (texture::title.width / 2);
	float titlePosY = (screenHeight / 2) - (screenWidth / 3.4f);

	float buttonsUp = screenWidth / 120;
	float starsUp = screenWidth / 120;
	float informationUp = (screenWidth / 30) + sizes::starsSize * 3;
	float middleButtonsPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);

	float changeCharacterPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);
	float changeCharacterPosY = screenWidth / 12;
	float girlCharacterPosX = (screenWidth / 2) - texture::girl2.width - (screenWidth / 12);
	float boyCharacterPosX = (screenWidth / 2) + (screenWidth / 12);
	float charactersPosY = (screenHeight / 2) - (screenWidth / 40);

	float backButtonPosX = (screenWidth / 2) - (texture::backButton.width / 2);
	float backButtonPosY = screenHeight - texture::backButton.height;

	float menuPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);
	float menuPosY = (screenHeight / 2) - (screenWidth / 6.6f);
	float menuPosY2 = (screenHeight / 2) - (screenWidth / 15);
	float menuPosY3 = (screenHeight / 2) + (screenWidth / 60);
	float menuPosY4 = (screenHeight / 2) + (screenWidth / 10);
	float menuPosY5 = (screenHeight / 2) + (screenWidth / 5.45f);

	float levelsPosX = screenWidth / 14;
	float levelsPosX2 = screenWidth / 20;
	float levelsPosX3 = screenWidth / 16;
	float levelsPosY = screenHeight / 15;

	float gamePosX = (screenWidth / 2) - texture::menuButton.width - (screenWidth / 12);
	float gamePosX2 = (screenWidth / 2) + (screenWidth / 12);
	float gamePosX3 = screenWidth - texture::settingsButton2.width - (screenWidth / 120);
	float gamePosY = screenHeight - sizes::buttonsHeight;
	float gamePosY2 = screenWidth / 100;
	float gamePosY3 = (screenWidth / 100) + (texture::replayLevelButton.height * 1.3f);

	float creditsPosX = screenWidth / 50;
	float creditsPosX2 = screenWidth - (screenWidth / 7.5f);
	float creditsPosX3 = screenWidth - (screenWidth / 4.8f);
	float creditsPosX4 = screenWidth - (screenWidth / 6);
	float creditsPosX5 = screenWidth - (screenWidth / 4.8f);
	float creditsPosY = (screenHeight / 2) - (screenWidth / 3.2f);
	float creditsPosY2 = (screenHeight / 2) - (screenWidth / 3.75f);
	float creditsPosY3 = (screenHeight / 2) - (screenWidth / 5.2f);
	float creditsPosY4 = (screenHeight / 2) - (screenWidth / 6.6f);
	float creditsPosY5 = (screenHeight / 2) - (screenWidth / 9.2f);
	float creditsPosY6 = (screenHeight / 2) - (screenWidth / 15);
	float creditsPosY7 = (screenHeight / 2) - (screenWidth / 40);
	float creditsPosY8 = (screenHeight / 2) + (screenWidth / 60);
	float creditsPosY9 = (screenHeight / 2) + (screenWidth / 17.14f);
	float creditsPosY10 = (screenHeight / 2) + (screenWidth / 10);
	float creditsPosY11 = (screenHeight / 2) + (screenWidth / 7.f);
	float creditsPosY12 = (screenHeight / 2) + (screenWidth / 5.45f);
	float creditsPosY13 = (screenHeight / 2) + (screenWidth / 4.4f);

	float settingsPosX = (screenWidth / 2) - (texture::audioButton.width / 2);
	float settingsPosX2 = (screenWidth / 2) - (texture::resolutionsButton.width / 2);
	float settingsPosY = (screenHeight / 2) - (screenWidth / 7.058f);
	float settingsPosY2 = (screenHeight / 2) - (screenWidth / 17.14f);
	float settingsPosY3 = (screenHeight / 2) + (screenWidth / 40);

	float winnerScreenPosX = (screenWidth / 2) - (texture::levelCompleteSquare.width / 2);
	float winnerScreenPosX2 = (screenWidth / 2) - (texture::levelCompleteText.width / 2);
	float winnerScreenPosX3 = (screenWidth / 2) - texture::nextLevelButton.width - (screenWidth / 24);
	float winnerScreenPosX4 = (screenWidth / 2) + (screenWidth / 24);
	float winnerScreenPosX5 = (screenWidth / 2) - texture::menuButton2.width - (screenWidth / 24);
	float winnerScreenPosX6 = (screenWidth / 2) + (screenWidth / 24);
	float winnerScreenPosY = (screenHeight / 2) - (texture::levelCompleteSquare.height / 2);
	float winnerScreenPosY2 = (screenHeight / 2) - (texture::levelCompleteText.height * 1.3f);
	float winnerScreenPosY3 = (screenHeight / 2);
	float winnerScreenPosY4 = (screenHeight / 2);
	float winnerScreenPosY5 = (screenHeight / 2) + (texture::menuButton2.height * 1.3f);
	float winnerScreenPosY6 = (screenHeight / 2) + (texture::levelsButton2.width * 1.3f);

	float resolutionPosY = (screenHeight / 2) - (screenWidth / 6);
	float resolutionPosY2 = (screenHeight / 2) - (screenWidth / 10);
	float resolutionPosY3 = (screenHeight / 2) - (screenWidth / 60);
	float resolutionPosY4 = (screenHeight / 2) + (screenWidth / 15);

	float audioPosY = (screenHeight / 2) - (screenWidth / 3);
	float audioPosY2 = (screenHeight / 2) - (screenWidth / 3.74f);
	float audioPosY3 = (screenHeight / 2) - (screenWidth / 5.45f);
	float audioPosY4 = (screenHeight / 2) - (screenWidth / 10);
	float audioPosY5 = (screenHeight / 2) - (screenWidth / 60);
	float audioPosY6 = (screenHeight / 2) + (screenWidth / 20);
	float audioPosY7 = (screenHeight / 2) + (screenWidth / 7.5f);
	float audioPosY8 = (screenHeight / 2) + (screenWidth / 4.6f);

	float instructionsPosY = (screenHeight / 2) - (screenWidth / 3);
	float instructionsPosY2 = (screenHeight / 2) - (screenWidth / 3.4f);
	float instructionsPosY3 = (screenHeight / 2) - (screenWidth / 4);
	float instructionsPosY4 = (screenHeight / 2) - (screenWidth / 6);
	float instructionsPosY5 = (screenHeight / 2) - (screenWidth / 8);
	float instructionsPosY6 = (screenHeight / 2) - (screenWidth / 25);
	float instructionsPosY7 = (screenHeight / 2);
	float instructionsPosY8 = (screenHeight / 2) + (screenWidth / 12);
	float instructionsPosY9 = (screenHeight / 2) + (screenWidth / 8);
	float instructionsPosY10 = (screenHeight / 2) + (screenWidth / 4.8f);
	float instructionsPosY11 = (screenHeight / 2) + (screenWidth / 4);

	float versionPosX = screenWidth / 50;
	float versionPosY = screenHeight - 40;

	float pausePosX = (screenWidth / 2) - MeasureText("Pause", sizes::textSize5) / 2;
	float pausePosY = screenWidth / 24;

	void updateVariables()
	{
		titlePosX = (screenWidth / 2) - (texture::title.width / 2);
		titlePosY = (screenHeight / 2) - (screenWidth / 3.4f);

		buttonsUp = screenWidth / 120;
		starsUp = screenWidth / 120;
		informationUp = (screenWidth / 30) + sizes::starsSize * 3;
		middleButtonsPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);

		changeCharacterPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);
		changeCharacterPosY = screenWidth / 12;
		girlCharacterPosX = (screenWidth / 2) - texture::girl2.width - (screenWidth / 12);
		boyCharacterPosX = (screenWidth / 2) + (screenWidth / 12);
		charactersPosY = (screenHeight / 2) - (screenWidth / 40);

		backButtonPosX = (screenWidth / 2) - (texture::backButton.width / 2);
		backButtonPosY = screenHeight - texture::backButton.height;

		menuPosX = (screenWidth / 2) - (sizes::buttonsWidth / 2);
		menuPosY = (screenHeight / 2) - (screenWidth / 6.6f);
		menuPosY2 = (screenHeight / 2) - (screenWidth / 15);
		menuPosY3 = (screenHeight / 2) + (screenWidth / 60);
		menuPosY4 = (screenHeight / 2) + (screenWidth / 10);
		menuPosY5 = (screenHeight / 2) + (screenWidth / 5.45f);

		levelsPosX = screenWidth / 14;
		levelsPosX2 = screenWidth / 20;
		levelsPosX3 = screenWidth / 16;
		levelsPosY = screenHeight / 15;

		gamePosX = (screenWidth / 2) - texture::menuButton.width - (screenWidth / 12);
		gamePosX2 = (screenWidth / 2) + (screenWidth / 12);
		gamePosX3 = screenWidth - texture::settingsButton2.width - (screenWidth / 120);
		gamePosY = screenHeight - sizes::buttonsHeight;
		gamePosY2 = screenWidth / 100;
		gamePosY3 = (screenWidth / 100) + (texture::replayLevelButton.height * 1.3f);

		creditsPosX = screenWidth / 50;
		creditsPosX2 = screenWidth - (screenWidth / 7.5f);
		creditsPosX3 = screenWidth - (screenWidth / 4.8f);
		creditsPosX4 = screenWidth - (screenWidth / 6);
		creditsPosX5 = screenWidth - (screenWidth / 4.8f);
		creditsPosY = (screenHeight / 2) - (screenWidth / 3.2f);
		creditsPosY2 = (screenHeight / 2) - (screenWidth / 3.75f);
		creditsPosY3 = (screenHeight / 2) - (screenWidth / 5.2f);
		creditsPosY4 = (screenHeight / 2) - (screenWidth / 6.6f);
		creditsPosY5 = (screenHeight / 2) - (screenWidth / 9.2f);
		creditsPosY6 = (screenHeight / 2) - (screenWidth / 15);
		creditsPosY7 = (screenHeight / 2) - (screenWidth / 40);
		creditsPosY8 = (screenHeight / 2) + (screenWidth / 60);
		creditsPosY9 = (screenHeight / 2) + (screenWidth / 17.14f);
		creditsPosY10 = (screenHeight / 2) + (screenWidth / 10);
		creditsPosY11 = (screenHeight / 2) + (screenWidth / 7.f);
		creditsPosY12 = (screenHeight / 2) + (screenWidth / 5.45f);
		creditsPosY13 = (screenHeight / 2) + (screenWidth / 4.4f);

		settingsPosX = (screenWidth / 2) - (texture::audioButton.width / 2);
		settingsPosX2 = (screenWidth / 2) - (texture::resolutionsButton.width / 2);
		settingsPosY = (screenHeight / 2) - (screenWidth / 7.058f);
		settingsPosY2 = (screenHeight / 2) - (screenWidth / 17.14f);
		settingsPosY3 = (screenHeight / 2) + (screenWidth / 40);

		winnerScreenPosX = (screenWidth / 2) - (texture::levelCompleteSquare.width / 2);
		winnerScreenPosX2 = (screenWidth / 2) - (texture::levelCompleteText.width / 2);
		winnerScreenPosX3 = (screenWidth / 2) - texture::nextLevelButton.width - (screenWidth / 24);
		winnerScreenPosX4 = (screenWidth / 2) + (screenWidth / 24);
		winnerScreenPosX5 = (screenWidth / 2) - texture::menuButton2.width - (screenWidth / 24);
		winnerScreenPosX6 = (screenWidth / 2) + (screenWidth / 24);
		winnerScreenPosY = (screenHeight / 2) - (texture::levelCompleteSquare.height / 2);
		winnerScreenPosY2 = (screenHeight / 2) - (texture::levelCompleteText.height * 1.3f);
		winnerScreenPosY3 = (screenHeight / 2);
		winnerScreenPosY4 = (screenHeight / 2);
		winnerScreenPosY5 = (screenHeight / 2) + (texture::menuButton2.height * 1.3f);
		winnerScreenPosY6 = (screenHeight / 2) + (texture::levelsButton2.width * 1.3f);

		resolutionPosY = (screenHeight / 2) - (screenWidth / 6);
		resolutionPosY2 = (screenHeight / 2) - (screenWidth / 10);
		resolutionPosY3 = (screenHeight / 2) - (screenWidth / 60);
		resolutionPosY4 = (screenHeight / 2) + (screenWidth / 15);

		audioPosY = (screenHeight / 2) - (screenWidth / 3);
		audioPosY2 = (screenHeight / 2) - (screenWidth / 3.74f);
		audioPosY3 = (screenHeight / 2) - (screenWidth / 5.45f);
		audioPosY4 = (screenHeight / 2) - (screenWidth / 10);
		audioPosY5 = (screenHeight / 2) - (screenWidth / 60);
		audioPosY6 = (screenHeight / 2) + (screenWidth / 20);
		audioPosY7 = (screenHeight / 2) + (screenWidth / 7.5f);
		audioPosY8 = (screenHeight / 2) + (screenWidth / 4.6f);

		instructionsPosY = (screenHeight / 2) - (screenWidth / 3);
		instructionsPosY2 = (screenHeight / 2) - (screenWidth / 3.4f);
		instructionsPosY3 = (screenHeight / 2) - (screenWidth / 4);
		instructionsPosY4 = (screenHeight / 2) - (screenWidth / 6);
		instructionsPosY5 = (screenHeight / 2) - (screenWidth / 8);
		instructionsPosY6 = (screenHeight / 2) - (screenWidth / 25);
		instructionsPosY7 = (screenHeight / 2);
		instructionsPosY8 = (screenHeight / 2) + (screenWidth / 12);
		instructionsPosY9 = (screenHeight / 2) + (screenWidth / 8);
		instructionsPosY10 = (screenHeight / 2) + (screenWidth / 4.8f);
		instructionsPosY11 = (screenHeight / 2) + (screenWidth / 4);

		versionPosX = screenWidth / 50;
		versionPosY = screenHeight - 40;

		pausePosX = (screenWidth / 2) - MeasureText("Pause", sizes::textSize5) / 2;
		pausePosY = screenWidth / 24;
	}
}