#ifndef POSITIONS_H
#define POSITIONS_H

#include "scenes/gameplay/gameplay.h"

namespace pos
{
	extern float titlePosX;
	extern float titlePosY;

	extern float buttonsUp;
	extern float starsUp;
	extern float informationUp;
	extern float middleButtonsPosX;

	extern float changeCharacterPosX;
	extern float changeCharacterPosY;
	extern float girlCharacterPosX;
	extern float boyCharacterPosX;
	extern float charactersPosY;

	extern float backButtonPosX;
	extern float backButtonPosY;

	extern float menuPosX;
	extern float menuPosY;
	extern float menuPosY2;
	extern float menuPosY3;
	extern float menuPosY4;
	extern float menuPosY5;

	extern float levelsPosX;
	extern float levelsPosX2;
	extern float levelsPosX3;
	extern float levelsPosY;

	extern float gamePosX;
	extern float gamePosX2;
	extern float gamePosX3;
	extern float gamePosY;
	extern float gamePosY2;
	extern float gamePosY3;

	extern float creditsPosX;
	extern float creditsPosX2;
	extern float creditsPosX3;
	extern float creditsPosX4;
	extern float creditsPosX5;
	extern float creditsPosY;
	extern float creditsPosY2;
	extern float creditsPosY3;
	extern float creditsPosY4;
	extern float creditsPosY5;
	extern float creditsPosY6;
	extern float creditsPosY7;
	extern float creditsPosY8;
	extern float creditsPosY9;
	extern float creditsPosY10;
	extern float creditsPosY11;
	extern float creditsPosY12;
	extern float creditsPosY13;

	extern float settingsPosX;
	extern float settingsPosX2;
	extern float settingsPosY;
	extern float settingsPosY2;
	extern float settingsPosY3;

	extern float winnerScreenPosX;
	extern float winnerScreenPosX2;
	extern float winnerScreenPosX3;
	extern float winnerScreenPosX4;
	extern float winnerScreenPosX5;
	extern float winnerScreenPosX6;
	extern float winnerScreenPosY;
	extern float winnerScreenPosY2;
	extern float winnerScreenPosY3;
	extern float winnerScreenPosY4;
	extern float winnerScreenPosY5;
	extern float winnerScreenPosY6;

	extern float resolutionPosY;
	extern float resolutionPosY2;
	extern float resolutionPosY3;
	extern float resolutionPosY4;

	extern float audioPosY;
	extern float audioPosY2;
	extern float audioPosY3;
	extern float audioPosY4;
	extern float audioPosY5;
	extern float audioPosY6;
	extern float audioPosY7;
	extern float audioPosY8;

	extern float instructionsPosY;
	extern float instructionsPosY2;
	extern float instructionsPosY3;
	extern float instructionsPosY4;
	extern float instructionsPosY5;
	extern float instructionsPosY6;
	extern float instructionsPosY7;
	extern float instructionsPosY8;
	extern float instructionsPosY9;
	extern float instructionsPosY10;
	extern float instructionsPosY11;

	extern float pausePosX;
	extern float pausePosY;

	extern float versionPosX;
	extern float versionPosY;

	void updateVariables();
}

#endif