#ifndef SIZES_H
#define SIZES_H

#include "scenes/gameplay/gameplay.h"

namespace sizes
{
	extern float entities;
	extern float framesEntities;

	extern float titleWidth;
	extern float titleHeight;

	extern float buttonsWidth;
	extern float buttonsHeight;

	extern float blocksWidth;
	extern float blocksHeight;

	extern float boxWidth;
	extern float boxHeight;

	extern float levelsSize;

	extern float starsSize;

	extern float settingsButton;

	extern float textSize;
	extern float textSize2;
	extern float textSize3;
	extern float textSize4;
	extern float textSize5;

	extern float entitiesWidth;
	extern float entitiesHeight;
	extern float titleEntitiesWidth;
	extern float titleEntitiesHeight;

	void updateSizes();
}

#endif