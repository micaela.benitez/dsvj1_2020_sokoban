#include "sizes.h"

using namespace sokoban;
using namespace gameplay;

namespace sizes
{
	float entities = screenWidth / 25;
	float framesEntities = 4;

	float titleWidth = screenWidth / 1.73f;
	float titleHeight = screenHeight / 6.8f;

	float buttonsWidth = screenWidth / 3;
	float buttonsHeight = screenHeight / 10;

	float blocksWidth = screenWidth / blocksPerLine;
	float blocksHeight = screenHeight / blocksPerColumn;

	float boxWidth = blocksWidth;
	float boxHeight = blocksHeight;

	float levelsSize = screenWidth / 6;

	float starsSize = screenWidth / 24;

	float settingsButton = screenWidth / 12;

	float textSize = screenWidth / 12;
	float textSize2 = screenWidth / 45;
	float textSize3 = screenWidth / 30;
	float textSize4 = screenWidth / 40;
	float textSize5 = screenWidth / 20;

	float entitiesWidth = screenWidth / 6.6f;
	float entitiesHeight = screenWidth / 8;
	float titleEntitiesWidth = screenHeight / 0.85f;
	float titleEntitiesHeight = screenHeight / 7.53f;

	void updateSizes()
	{  
		titleWidth = screenWidth / 1.73f;
		titleHeight = screenHeight / 6.8f;

		buttonsWidth = screenWidth / 3;
		buttonsHeight = screenHeight / 10;

		blocksWidth = screenWidth / blocksPerLine;
		blocksHeight = screenHeight / blocksPerColumn;

		boxWidth = blocksWidth;
		boxHeight = blocksHeight;

		levelsSize = screenWidth / 6;

		starsSize = screenWidth / 24;

		settingsButton = screenWidth / 12;

		textSize = screenWidth / 12;   // 100
		textSize2 = screenWidth / 45;   // 26,6
		textSize3 = screenWidth / 30;   // 40
		textSize4 = screenWidth / 40;   // 30
		textSize5 = screenWidth / 20;   // 60

		entitiesWidth = screenWidth / 6.6f;
		entitiesHeight = screenWidth / 8;
		titleEntitiesWidth = screenHeight / 0.85f;
		titleEntitiesHeight = screenHeight / 7.53f;
	}
}