#include "condition_to_win.h"

using namespace sokoban;
using namespace gameplay;

void conditionToWin()
{
	int boxUp = 0;
	character.lvlWon = true;

	for (int i = 0; i < crossesAvailable; i++)
	{
		for (int j = 0; j < boxesAvailable; j++)
		{
			if (cross[i].position.x == box[j].position.x && cross[i].position.y == box[j].position.y && box[j].available) boxUp++;
		}

		if (boxUp == 0) character.lvlWon = false;
		else boxUp = 0;
	}
}

void result()
{
	level[actualLevel].completed = true;

	for (int i = 0; i < quantityLevels; i++)
	{
		if (level[i].actualMoves < level[i].moves) level[i].moves = level[i].actualMoves;
	}
}