#ifndef ENTITIES_H
#define ENTITIES_H

#include "raylib.h"

enum class Tiles
{ 
	GRASS, 
	WALL, 
	FLOOR, 
	HOLE,
	BUTTON,
	UPREDDOOR,
	DOWNREDDOOR,
	LEFTREDDOOR,
	RIGHTREDDOOR,
	UPBROWNDOOR,
	DOWNBROWNDOOR,
	LEFTBROWNDOOR,
	RIGHTBROWNDOOR,
	WATER
};

struct Character
{
	Vector2 position;
	Vector2 posBlock;
	Texture2D texture;
	bool boxDown;
	bool boxLeft;
	bool boxUp;
	bool boxRight;
	bool lvlWon;
	bool loser;
};

struct Box
{
	Vector2 position;
	bool available;
};

struct Key
{
	Vector2 position;
	bool available;
	bool taken;
};

struct Block
{
	Vector2 position;
	Tiles tile;
	Texture2D texture;
	bool empty;
};

struct Level
{
	Vector2 position;
	bool completed;
	bool available;
	Color color;
	Color star1;
	Color star2;
	Color star3;
	int star1Moves;
	int star2Moves;
	int star3Moves;
	int actualMoves;
	int moves;
	Texture2D texture;
};

#endif