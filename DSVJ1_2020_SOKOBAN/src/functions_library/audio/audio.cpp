#include "audio.h"

using namespace sokoban;
using namespace gameplay;

namespace audio
{
	GameMusic gameMusic;
	float musicVolume = 1.0f;

	GameSounds gameSounds;
	float soundVolume = 4.0f;

	void menuAudio()
	{
		UpdateMusicStream(gameMusic.menu);

		if (sounds.button) PlaySoundMulti(gameSounds.button);
	}

	void gameplayAudio()
	{
		UpdateMusicStream(gameMusic.gameplay);

		if (sounds.characterMoving) PlaySoundMulti(gameSounds.characterMoving);
		if (sounds.button) PlaySoundMulti(gameSounds.button);
		if (sounds.keyHole) PlaySoundMulti(gameSounds.keyHole);
		if (sounds.redButton) PlaySoundMulti(gameSounds.redButton);
	}

	void loadSounds()
	{
		gameSounds.characterMoving = LoadSound("res/assets/audio/sounds/charactermoving.wav");
		gameSounds.button = LoadSound("res/assets/audio/sounds/button.wav");
		gameSounds.keyHole = LoadSound("res/assets/audio/sounds/keyhole.wav");
		gameSounds.redButton = LoadSound("res/assets/audio/sounds/redButton.wav");
	}

	void setSoundsVolume()
	{
		SetSoundVolume(gameSounds.characterMoving, soundVolume);
		SetSoundVolume(gameSounds.button, soundVolume);
		SetSoundVolume(gameSounds.keyHole, soundVolume);
		SetSoundVolume(gameSounds.redButton, soundVolume);
	}

	void unloadSounds()
	{
		UnloadSound(gameSounds.characterMoving);
		UnloadSound(gameSounds.button);
		UnloadSound(gameSounds.keyHole);
		UnloadSound(gameSounds.redButton);
	}

	void loadMusic()
	{
		gameMusic.menu = LoadMusicStream("res/assets/audio/music/menumusic.mp3");
		gameMusic.gameplay = LoadMusicStream("res/assets/audio/music/gamemusic.mp3");
	}

	void setMusicVolume()
	{
		SetMusicVolume(gameMusic.menu, musicVolume);
		SetMusicVolume(gameMusic.gameplay, musicVolume);
	}

	void unloadMusic()
	{
		UnloadMusicStream(gameMusic.menu);
		UnloadMusicStream(gameMusic.gameplay);
	}
}