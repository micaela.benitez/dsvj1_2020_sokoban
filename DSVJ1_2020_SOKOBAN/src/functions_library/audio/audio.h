#ifndef AUDIO_H
#define AUDIO_H

#include "scenes/gameplay/gameplay.h"

namespace audio
{
	struct GameSounds
	{
		Sound characterMoving;
		Sound button;
		Sound keyHole;
		Sound redButton;
	};

	struct GameMusic
	{
		Music menu;
		Music gameplay;
	};

	extern GameMusic gameMusic;
	extern float musicVolume;

	extern GameSounds gameSounds;
	extern float soundVolume;

	extern void menuAudio();
	extern void gameplayAudio();

	extern void loadSounds();
	extern void setSoundsVolume();
	extern void unloadSounds();

	extern void loadMusic();
	extern void setMusicVolume();
	extern void unloadMusic();
}

#endif