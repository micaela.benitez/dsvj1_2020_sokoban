#ifndef LEVELS_GENERATOR_H
#define LEVELS_GENERATOR_H

#include "scenes/gameplay/gameplay.h"

void textures();
void loadLevels();
void SaveScore();
void LoadScore();
void starsPerLevels();
void entitiesLocation();

#endif