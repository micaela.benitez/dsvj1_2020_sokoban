#include "levels_generator.h"

using namespace sokoban;
using namespace gameplay;

void textures()
{
	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			if (actualLevel < 5)
			{
				if (block[i][j].tile == Tiles::GRASS) block[i][j].texture = texture::grass;
				else if (block[i][j].tile == Tiles::WALL) block[i][j].texture = texture::wall;
				else if (block[i][j].tile == Tiles::FLOOR) block[i][j].texture = texture::floor;
				else if (block[i][j].tile == Tiles::HOLE) block[i][j].texture = texture::hole;
				else if (block[i][j].tile == Tiles::BUTTON) block[i][j].texture = texture::redButton;
				else if (block[i][j].tile == Tiles::UPREDDOOR) block[i][j].texture = texture::upRedDoor;
				else if (block[i][j].tile == Tiles::DOWNREDDOOR) block[i][j].texture = texture::downRedDoor;
				else if (block[i][j].tile == Tiles::LEFTREDDOOR) block[i][j].texture = texture::leftRedDoor;
				else if (block[i][j].tile == Tiles::RIGHTREDDOOR) block[i][j].texture = texture::rightRedDoor;
				else if (block[i][j].tile == Tiles::UPBROWNDOOR) block[i][j].texture = texture::upBrownDoor;
				else if (block[i][j].tile == Tiles::DOWNBROWNDOOR) block[i][j].texture = texture::downBrownDoor;
				else if (block[i][j].tile == Tiles::LEFTBROWNDOOR) block[i][j].texture = texture::leftBrownDoor;
				else if (block[i][j].tile == Tiles::RIGHTBROWNDOOR) block[i][j].texture = texture::rightBrownDoor;
				else if (block[i][j].tile == Tiles::WATER) block[i][j].texture = texture::water;
			}
			else
			{
				if (block[i][j].tile == Tiles::GRASS) block[i][j].texture = texture::grassNight;
				else if (block[i][j].tile == Tiles::WALL) block[i][j].texture = texture::wallNight;
				else if (block[i][j].tile == Tiles::FLOOR) block[i][j].texture = texture::floorNight;
				else if (block[i][j].tile == Tiles::HOLE) block[i][j].texture = texture::holeNight;
				else if (block[i][j].tile == Tiles::BUTTON) block[i][j].texture = texture::redButtonNight;
				else if (block[i][j].tile == Tiles::UPREDDOOR) block[i][j].texture = texture::upRedDoorNight;
				else if (block[i][j].tile == Tiles::DOWNREDDOOR) block[i][j].texture = texture::downRedDoorNight;
				else if (block[i][j].tile == Tiles::LEFTREDDOOR) block[i][j].texture = texture::leftRedDoorNight;
				else if (block[i][j].tile == Tiles::RIGHTREDDOOR) block[i][j].texture = texture::rightRedDoorNight;
				else if (block[i][j].tile == Tiles::UPBROWNDOOR) block[i][j].texture = texture::upBrownDoorNight;
				else if (block[i][j].tile == Tiles::DOWNBROWNDOOR) block[i][j].texture = texture::downBrownDoorNight;
				else if (block[i][j].tile == Tiles::LEFTBROWNDOOR) block[i][j].texture = texture::leftBrownDoorNight;
				else if (block[i][j].tile == Tiles::RIGHTBROWNDOOR) block[i][j].texture = texture::rightBrownDoorNight;
				else if (block[i][j].tile == Tiles::WATER) block[i][j].texture = texture::water;
			}
		}
	}
}

void loadLevels()
{
	int auxDataIndex = 0;
	auxDataIndex = quantityBlocksPerLevel * actualLevel;

	for (int i = 0; i < blocksPerLine; i++)
	{
		for (int j = 0; j < blocksPerColumn; j++)
		{
			block[i][j].tile = static_cast<Tiles>(LoadStorageValue(auxDataIndex));
			auxDataIndex++;
		}
	}
}

void SaveScore()
{
	int auxDataIndex = 0;
	auxDataIndex = (quantityBlocksPerLevel * maxLevel) + quantityBlocksPerLevel;

	for (int i = 0; i < 10; i++)
	{
		SaveStorageValue(auxDataIndex, level[i].moves);
		auxDataIndex++;
		SaveStorageValue(auxDataIndex, level[i].completed);
		auxDataIndex++;
	}
}

void LoadScore()
{
	int auxDataIndex = 0;
	auxDataIndex = (quantityBlocksPerLevel * maxLevel) + quantityBlocksPerLevel;

	for (int i = 0; i < 10; i++)
	{
		level[i].moves = (LoadStorageValue(auxDataIndex));
		auxDataIndex++;
		level[i].completed = (LoadStorageValue(auxDataIndex));
		auxDataIndex++;
	}
}

void starsPerLevels()
{
	if (actualLevel == 0)
	{
		level[0].star3Moves = 18;
		level[0].star2Moves = 20;
		level[0].star1Moves = 22;
	}
	else if (actualLevel == 1)
	{
		level[1].star3Moves = 70;
		level[1].star2Moves = 72;
		level[1].star1Moves = 74;
	}
	else if (actualLevel == 2)
	{
		level[2].star3Moves = 34;
		level[2].star2Moves = 36;
		level[2].star1Moves = 38;
	}
	else if (actualLevel == 3)
	{
		level[3].star3Moves = 58;
		level[3].star2Moves = 60;
		level[3].star1Moves = 62;
	}
	else if (actualLevel == 4)
	{
		level[4].star3Moves = 336;
		level[4].star2Moves = 334;
		level[4].star1Moves = 332;
	}
	else if (actualLevel == 5)
	{
		level[5].star3Moves = 247;
		level[5].star2Moves = 265;
		level[5].star1Moves = 263;
	}
	else if (actualLevel == 6)
	{
		level[6].star3Moves = 110;
		level[6].star2Moves = 108;
		level[6].star1Moves = 106;
	}
	else if (actualLevel == 7)
	{
		level[7].star3Moves = 363;
		level[7].star2Moves = 361;
		level[7].star1Moves = 359;
	}
	else if (actualLevel == 8)
	{
		level[8].star3Moves = 248;
		level[8].star2Moves = 246;
		level[8].star1Moves = 244;
	}
	else if (actualLevel == 9)
	{
		level[9].star3Moves = 603;
		level[9].star2Moves = 601;
		level[9].star1Moves = 599;
	}
}

void entitiesLocation()
{
	if (actualLevel == 0)
	{
		character.position = { block[9][8].position.x, block[9][8].position.y };
		character.posBlock = { 9, 8 };

		boxesAvailable = 4;
		box[0].position = { block[7][6].position.x, block[7][6].position.y };
		box[1].position = { block[7][10].position.x, block[7][10].position.y };
		box[2].position = { block[11][6].position.x, block[11][6].position.y };
		box[3].position = { block[11][10].position.x, block[11][10].position.y };

		crossesAvailable = 4;
		cross[0].position = { block[7][5].position.x, block[7][5].position.y };
		cross[1].position = { block[7][11].position.x, block[7][11].position.y };
		cross[2].position = { block[11][5].position.x, block[11][5].position.y };
		cross[3].position = { block[11][11].position.x, block[11][11].position.y };
	}
	else if (actualLevel == 1)
	{
		character.position = { block[8][5].position.x, block[8][5].position.y };
		character.posBlock = { 8, 5 };

		boxesAvailable = 3;
		box[0].position = { block[8][7].position.x, block[8][7].position.y };
		box[1].position = { block[7][9].position.x, block[7][9].position.y };
		box[2].position = { block[10][8].position.x, block[10][8].position.y };

		crossesAvailable = 3;
		cross[0].position = { block[9][5].position.x, block[9][5].position.y };
		cross[1].position = { block[10][5].position.x, block[10][5].position.y };
		cross[2].position = { block[11][5].position.x, block[11][5].position.y };
	}
	else if (actualLevel == 2)
	{
		character.position = { block[8][6].position.x, block[8][6].position.y };
		character.posBlock = { 8, 6 };

		boxesAvailable = 6;
		box[0].position = { block[9][6].position.x, block[9][6].position.y };
		box[1].position = { block[10][7].position.x, block[10][7].position.y };
		box[2].position = { block[10][8].position.x, block[10][8].position.y };
		box[3].position = { block[7][10].position.x, block[7][10].position.y };
		box[4].position = { block[10][10].position.x, block[10][10].position.y };
		box[5].position = { block[11][10].position.x, block[11][10].position.y };

		crossesAvailable = 5;
		cross[0].position = { block[7][6].position.x, block[7][6].position.y };
		cross[1].position = { block[7][8].position.x, block[7][8].position.y };
		cross[2].position = { block[10][9].position.x, block[10][9].position.y };
		cross[3].position = { block[10][11].position.x, block[10][11].position.y };
		cross[4].position = { block[12][10].position.x, block[12][10].position.y };
	}
	else if (actualLevel == 3)
	{
		character.position = { block[9][9].position.x, block[9][9].position.y };
		character.posBlock = { 9, 9 };

		boxesAvailable = 9;
		box[0].position = { block[7][9].position.x, block[7][9].position.y };
		box[1].position = { block[6][8].position.x, block[6][8].position.y };
		box[2].position = { block[7][7].position.x, block[7][7].position.y };
		box[3].position = { block[8][6].position.x, block[8][6].position.y };
		box[4].position = { block[9][5].position.x, block[9][5].position.y };
		box[5].position = { block[10][6].position.x, block[10][6].position.y };
		box[6].position = { block[11][7].position.x, block[11][7].position.y };
		box[7].position = { block[12][8].position.x, block[12][8].position.y };
		box[8].position = { block[11][9].position.x, block[11][9].position.y };

		crossesAvailable = 7;
		cross[0].position = { block[6][9].position.x, block[6][9].position.y };
		cross[1].position = { block[6][7].position.x, block[6][7].position.y };
		cross[2].position = { block[8][5].position.x, block[8][5].position.y };
		cross[3].position = { block[9][6].position.x, block[9][6].position.y };
		cross[4].position = { block[10][5].position.x, block[10][5].position.y };
		cross[5].position = { block[12][7].position.x, block[12][7].position.y };
		cross[6].position = { block[12][9].position.x, block[12][9].position.y };
	}
	else if (actualLevel == 4)
	{
		character.position = { block[8][5].position.x, block[8][5].position.y };
		character.posBlock = { 8, 5 };

		boxesAvailable = 12;
		box[0].position = { block[5][5].position.x, block[5][5].position.y };
		box[1].position = { block[6][11].position.x, block[6][11].position.y };
		box[2].position = { block[8][7].position.x, block[8][7].position.y };
		box[3].position = { block[9][11].position.x, block[9][11].position.y };
		box[4].position = { block[10][6].position.x, block[10][6].position.y };
		box[5].position = { block[11][9].position.x, block[11][9].position.y };
		box[6].position = { block[11][10].position.x, block[11][10].position.y };
		box[7].position = { block[11][11].position.x, block[11][11].position.y };
		box[8].position = { block[12][6].position.x, block[12][6].position.y };
		box[9].position = { block[12][10].position.x, block[12][10].position.y };
		box[10].position = { block[13][7].position.x, block[13][7].position.y };
		box[11].position = { block[13][11].position.x, block[13][11].position.y };

		crossesAvailable = 10;
		cross[0].position = { block[3][5].position.x, block[3][5].position.y };
		cross[1].position = { block[3][6].position.x, block[3][6].position.y };
		cross[2].position = { block[3][7].position.x, block[3][7].position.y };
		cross[3].position = { block[3][8].position.x, block[3][8].position.y };
		cross[4].position = { block[3][9].position.x, block[3][9].position.y };
		cross[5].position = { block[4][5].position.x, block[4][5].position.y };
		cross[6].position = { block[4][6].position.x, block[4][6].position.y };
		cross[7].position = { block[4][7].position.x, block[4][7].position.y };
		cross[8].position = { block[4][8].position.x, block[4][8].position.y };
		cross[9].position = { block[4][9].position.x, block[4][9].position.y };
	}
	else if (actualLevel == 5)
	{
		character.position = { block[13][10].position.x, block[13][10].position.y };
		character.posBlock = { 13, 10 };

		boxesAvailable = 8;
		box[0].position = { block[3][10].position.x, block[3][10].position.y };
		box[1].position = { block[6][5].position.x, block[6][5].position.y };
		box[2].position = { block[6][7].position.x, block[6][7].position.y };
		box[3].position = { block[6][10].position.x, block[6][10].position.y };
		box[4].position = { block[8][6].position.x, block[8][6].position.y };
		box[5].position = { block[9][7].position.x, block[9][7].position.y };
		box[6].position = { block[13][9].position.x, block[13][9].position.y };
		box[7].position = { block[15][10].position.x, block[15][10].position.y };

		crossesAvailable = 6;
		cross[0].position = { block[15][9].position.x, block[15][9].position.y };
		cross[1].position = { block[15][10].position.x, block[15][10].position.y };
		cross[2].position = { block[15][11].position.x, block[15][11].position.y };
		cross[3].position = { block[16][9].position.x, block[16][9].position.y };
		cross[4].position = { block[16][10].position.x, block[16][10].position.y };
		cross[5].position = { block[16][11].position.x, block[16][11].position.y };
	}
	else if (actualLevel == 6)
	{
		character.position = { block[9][10].position.x, block[9][10].position.y };
		character.posBlock = { 9, 10 };

		key.available = true;
		key.position = { block[13][10].position.x, block[13][10].position.y };

		boxesAvailable = 7;
		box[0].position = { block[8][6].position.x, block[8][6].position.y };
		box[1].position = { block[9][7].position.x, block[9][7].position.y };
		box[2].position = { block[9][8].position.x, block[9][8].position.y };
		box[3].position = { block[9][9].position.x, block[9][9].position.y };
		box[4].position = { block[12][7].position.x, block[12][7].position.y };
		box[5].position = { block[12][8].position.x, block[12][8].position.y };
		box[6].position = { block[12][9].position.x, block[12][9].position.y };

		crossesAvailable = 6;
		cross[0].position = { block[6][6].position.x, block[6][6].position.y };
		cross[1].position = { block[9][6].position.x, block[9][6].position.y };
		cross[2].position = { block[11][6].position.x, block[11][6].position.y };
		cross[3].position = { block[12][5].position.x, block[12][5].position.y };
		cross[4].position = { block[12][6].position.x, block[12][6].position.y };
		cross[5].position = { block[13][5].position.x, block[13][5].position.y };
	}	
	else if (actualLevel == 7)
	{
		character.position = { block[15][4].position.x, block[15][4].position.y };
		character.posBlock = { 15, 4 };

		key.available = true;
		key.position = { block[12][6].position.x, block[12][6].position.y };

		boxesAvailable = 12;
		box[0].position = { block[10][4].position.x, block[10][4].position.y };
		box[1].position = { block[10][10].position.x, block[10][10].position.y };
		box[2].position = { block[11][5].position.x, block[11][5].position.y };
		box[3].position = { block[11][6].position.x, block[11][6].position.y };
		box[4].position = { block[11][7].position.x, block[11][7].position.y };
		box[5].position = { block[11][8].position.x, block[11][8].position.y };
		box[6].position = { block[11][9].position.x, block[11][9].position.y };
		box[7].position = { block[13][5].position.x, block[13][5].position.y };
		box[8].position = { block[13][7].position.x, block[13][7].position.y };
		box[9].position = { block[13][10].position.x, block[13][10].position.y };
		box[10].position = { block[14][6].position.x, block[14][6].position.y };
		box[11].position = { block[14][9].position.x, block[14][9].position.y };

		crossesAvailable = 11;
		cross[0].position = { block[2][9].position.x, block[2][9].position.y };
		cross[1].position = { block[2][11].position.x, block[2][11].position.y };
		cross[2].position = { block[3][9].position.x, block[3][9].position.y };
		cross[3].position = { block[3][10].position.x, block[3][10].position.y };
		cross[4].position = { block[3][11].position.x, block[3][11].position.y };
		cross[5].position = { block[4][9].position.x, block[4][9].position.y };
		cross[6].position = { block[4][10].position.x, block[4][10].position.y };
		cross[7].position = { block[4][11].position.x, block[4][11].position.y };
		cross[8].position = { block[5][9].position.x, block[5][9].position.y };
		cross[9].position = { block[5][10].position.x, block[5][10].position.y };
		cross[10].position = { block[5][11].position.x, block[5][11].position.y };
	}
	else if (actualLevel == 8)
	{
		character.position = { block[11][7].position.x, block[11][7].position.y };
		character.posBlock = { 11, 7 };

		key.available = true;
		key.position = { block[10][12].position.x, block[10][12].position.y };

		boxesAvailable = 9;
		box[0].position = { block[6][4].position.x, block[6][4].position.y };
		box[1].position = { block[7][8].position.x, block[7][8].position.y };
		box[2].position = { block[8][6].position.x, block[8][6].position.y };
		box[3].position = { block[8][9].position.x, block[8][9].position.y };
		box[4].position = { block[8][11].position.x, block[8][11].position.y };
		box[5].position = { block[9][10].position.x, block[9][10].position.y };
		box[6].position = { block[10][4].position.x, block[10][4].position.y };
		box[7].position = { block[10][7].position.x, block[10][7].position.y };
		box[8].position = { block[13][10].position.x, block[13][10].position.y };

		crossesAvailable = 8;
		cross[0].position = { block[4][8].position.x, block[4][8].position.y };
		cross[1].position = { block[4][9].position.x, block[4][9].position.y };
		cross[2].position = { block[4][10].position.x, block[4][10].position.y };
		cross[3].position = { block[4][11].position.x, block[4][11].position.y };
		cross[4].position = { block[4][12].position.x, block[4][12].position.y };
		cross[5].position = { block[5][8].position.x, block[5][8].position.y };
		cross[6].position = { block[5][9].position.x, block[5][9].position.y };
		cross[7].position = { block[5][10].position.x, block[5][10].position.y };
	}
	else if (actualLevel == 9)
	{
		character.position = { block[13][4].position.x, block[13][4].position.y };
		character.posBlock = { 13, 4 };

		key.available = true;
		key.position = { block[1][5].position.x, block[1][5].position.y };

		boxesAvailable = 7;
		box[0].position = { block[2][12].position.x, block[2][12].position.y };
		box[1].position = { block[2][13].position.x, block[2][13].position.y };
		box[2].position = { block[3][10].position.x, block[3][10].position.y };
		box[3].position = { block[4][6].position.x, block[4][6].position.y };
		box[4].position = { block[4][8].position.x, block[4][8].position.y };
		box[5].position = { block[4][11].position.x, block[4][11].position.y };
		box[6].position = { block[4][12].position.x, block[4][12].position.y };

		crossesAvailable = 6;
		cross[0].position = { block[14][6].position.x, block[14][6].position.y };
		cross[1].position = { block[14][8].position.x, block[14][8].position.y };
		cross[2].position = { block[15][3].position.x, block[15][3].position.y };
		cross[3].position = { block[15][4].position.x, block[15][4].position.y };
		cross[4].position = { block[15][6].position.x, block[15][6].position.y };
		cross[5].position = { block[15][8].position.x, block[15][8].position.y };
	}

	for (int i = 0; i < boxesAvailable; i++) box[i].available = true;
}